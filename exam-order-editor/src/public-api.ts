/*
 * Public API Surface of exam-order-editor
 */

export * from './lib/exam-order-editor.service';
export * from './lib/exam-order-editor.component';
