import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'his-exam-order-editor',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './exam-order-editor.component.html',
  styleUrls: ['./exam-order-editor.component.scss'],
})
export class ExamOrderEditorComponent {

}
