/*
 * Public API Surface of soap-note-editor
 */

export * from './lib/soap-note-editor.service';
export * from './lib/soap-note-editor.component';
