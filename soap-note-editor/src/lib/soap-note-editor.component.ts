import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'his-soap-note-editor',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './soap-note-editor.component.html',
  styleUrls: ['./soap-note-editor.component.scss'],
})
export class SoapNoteEditorComponent {

}
