import { Injectable, WritableSignal, inject, signal } from "@angular/core";
import { OpdOrderEditorService } from "./opd-order-editor.service";
import { KeyOptions } from "@his-viewmodel/med-order/dist";

@Injectable({
  providedIn: 'root'
})
export class OpdOrderEditorValue {

  options: WritableSignal<KeyOptions> = signal({});
}
