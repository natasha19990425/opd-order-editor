import { Injectable, inject } from '@angular/core';
import { mockMenuTitles, mockOpdMedOrder } from './../../../app-showcase/src/assets/mock/mock-data';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { Observable, of } from 'rxjs';
import { MedBasic, MedTree, OpdMedOrder, DrugOrder, SearchReq, KeyOptions } from '@his-viewmodel/med-order';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { Coding, Quantity } from '@his-base/datatypes/dist';
import { SharedService } from '@his-base/shared';
import { OpdOrderEditorValue } from './opd-order-editor';

@Injectable({
  providedIn: 'root'
})
export class OpdOrderEditorService {

  #searchReq: SearchReq;

  #jetStreamWsService = inject(JetStreamWsService);
  #sharedService = inject(SharedService);

  valueService = inject(OpdOrderEditorValue);
  constructor() {
    this.#searchReq = {
      branch: this.#sharedService.appPortal?.branch as string,
      date: this.#sharedService.appPortal?.orderTime as Date,
    };
  }

  getMenuTitles(): Observable<MenuTitle[]> {
    // TODO: 跟後端串接資料
    return of(mockMenuTitles);
  }

  /** 取得目前的就診詳細資料
   * @returns
   */
  getOpdMedOrder(): Observable<OpdMedOrder> {
    // TODO: 需要監聽病患切換的事件
    // TODO: 跟後端串接資料
    return of(mockOpdMedOrder);
  }

  async getDefaultOptions() {
    return await this.#jetStreamWsService.request('medOrder.medFreqTimes.find', { 'keys': ['drug'] });
  }

  async getMedTree(code: string): Promise<MedTree> {
    return await this.#jetStreamWsService.request('medOrder.catalog.medTree', code);
  }

  async getMedBasics(codings: Coding[]): Promise<MedBasic[]> {

    if (codings.length) {
      return await this.#jetStreamWsService.request('medOrder.catalog.medBasic', codings);
    }
    return [];
  }

  async getMedBasic(codes: Coding[]): Promise<MedBasic[]> {
    return await this.#jetStreamWsService.request("medOrder.catalog.medBasic", codes);
  }

  parseDrugOrders(medBasics: MedBasic[]): DrugOrder[] {
    return medBasics.map((x: MedBasic) => {
      let { _id, code, dosage: dosageValue, dosageUnit, dose: doseValue, doseUnit, name: display, ...copy } = x;
      const dosage = new Quantity({ value: dosageValue, unit: dosageUnit });
      const dose = new Quantity({ value: doseValue, unit: doseUnit });
      const drug = new Coding({ code, display });
      _id = crypto.randomUUID();
      return new DrugOrder({ ...copy, dosage, dose, drug, _id });
    });
  }

  async initOptions() {
    const keys = ["units", "useDays"];

    const option = await this.reqOption(new SearchReq({ branch: '', date: new Date(), keys }));
    this.valueService.options.set(option);
  }
  /** 取得所有Option
   *  @param medType
   */
  async reqOption(searchReq: SearchReq): Promise<KeyOptions> {

    return await this.#jetStreamWsService.request('medOrder.option.find', searchReq);
  }
}
