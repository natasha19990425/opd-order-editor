import { OpdOrderEditorService } from './opd-order-editor.service';
import { AfterViewInit, Component, OnInit, WritableSignal, effect, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { HeaderComponent } from '@his-directive/header/dist/header';
import { DialogModule } from 'primeng/dialog';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { ToggleButtonModule } from 'primeng/togglebutton';
import { TabMenuModule } from 'primeng/tabmenu';
import { SideMenuComponent } from '@his-directive/side-menu/dist/side-menu';
import { TabViewModule } from 'primeng/tabview';
import { DividerModule } from 'primeng/divider';
import { SidebarModule } from 'primeng/sidebar';
import { MenuItem } from 'primeng/api';
import { SplitterModule } from 'primeng/splitter';
import { ButtonModule } from 'primeng/button';
import { SplitButtonModule } from 'primeng/splitbutton';
import { DropdownModule } from 'primeng/dropdown';
import { SelectButtonModule } from 'primeng/selectbutton';
import { ToolBarComponent } from '@his-directive/tool-bar/dist/tool-bar';
import { InfoSidebarComponent } from '@his-directive/info-sidebar/dist/info-sidebar';
import { DrugOrderEditorComponent, DrugOrderEditorService } from '../../../drug-order-editor/src/public-api';
import { DrugOrder, MedTree, OpdMedOrder } from '@his-viewmodel/med-order';
import { SharedService } from '@his-base/shared';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import '@angular/localize/init';
import { SoapNoteEditorComponent } from '../../../soap-note-editor/src/public-api';
import { DiagOrderEditorComponent } from '../../../diag-order-editor/src/public-api';
import { ExamOrderEditorComponent } from '../../../exam-order-editor/src/public-api';
import { Coding } from '@his-base/datatypes/dist';
import { SlidewayCartComponent } from "../../../slideway-cart/src/lib/slideway-cart.component";
import { INIT_CONNECT } from 'app-showcase/src/app/app.config';
const primeGroup = [ButtonModule, SplitterModule, SplitButtonModule, DropdownModule, TabViewModule, DividerModule, SidebarModule, DialogModule, FormsModule,
  CheckboxModule, ScrollPanelModule, SelectButtonModule, ToggleButtonModule, TabMenuModule];

@Component({
  selector: 'his-opd-order-editor',
  standalone: true,
  templateUrl: './opd-order-editor.component.html',
  styleUrls: ['./opd-order-editor.component.scss'],
  imports: [CommonModule, RouterOutlet, TranslateModule, primeGroup, SideMenuComponent, InfoSidebarComponent, DrugOrderEditorComponent, ToolBarComponent, HeaderComponent,
    SoapNoteEditorComponent, DiagOrderEditorComponent, ExamOrderEditorComponent, SlidewayCartComponent]
})
export class OpdOrderEditorComponent implements OnInit {

  selectOptions: any[] | undefined;
  selectValue: any;
  togBtnChecked: any;

  showTemplate: string = 'grid';
  menuTitles: MenuTitle[] = [];
  isSideMenuVisible = true;
  opdMedOrder!: OpdMedOrder;
  dittoItem!: MenuItem[];
  drugOrdersBackup: DrugOrder[] = [];
  signalDrugOrders: WritableSignal<DrugOrder[]> = signal([]);
  completeTime!: Date;
  sidebarExpanded = false;
  panelSizes = [83, 17]; // 初始為完全收合的尺寸

  isSlidewayCartVisible: boolean = false;
  drugTree: MedTree = {} as MedTree;
  defaultDrugFrequency: Coding = {} as Coding;
  defaultDrugUsedDays: number = 0;
  frequencyOptions: Coding[] = [];
  usedDaysOptions: number[] = [];

  #sharedService = inject(SharedService);
  #translate: TranslateService = inject(TranslateService);
  #opdOrderService = inject(OpdOrderEditorService);
  drugOrderEditorService = inject(DrugOrderEditorService);
  connectState = inject(INIT_CONNECT);
  constructor() {

    this.#translate.setDefaultLang(`zh-Hant`);
  }

  completeConnect = effect(async ()=>{
    if (this.connectState()) {
      const options = await this.#opdOrderService.getDefaultOptions();
      this.frequencyOptions = options['drug'];
      this.usedDaysOptions = [1, 2, 3, 4, 5, 6, 7, 14, 15, 28];
      this.#opdOrderService.initOptions();
    }
  });

  async ngOnInit() {

    this.completeTime = new Date();

    // this.#opdOrderService.getOpdMedOrder().subscribe((x) => {
    //   this.opdMedOrder = x;
    // });
    // this.drugOrdersBackup = structuredClone(this.opdMedOrder.drugOrders);
    this.#initOptions();
  }

  async #initOptions() {
    this.#opdOrderService.getMenuTitles().subscribe((x) => {
      this.menuTitles = x;
    });
    // this.signalDrugOrders.set(this.opdMedOrder.drugOrders);
    this.dittoItem = [
      {
        label: this.#translate.instant(`醫師上次`),
        command: () => {
          this.onDittoLastDoctor();
        },
      },
      {
        label: this.#translate.instant(`科別上次`),
        command: () => {
          this.onDittoLastDevision();
        },
      },
      {
        label: this.#translate.instant(`詳細選擇`),
        command: () => {
          this.onDittoDetailClick();
        },
      },
      { separator: true },
      {
        label: this.#translate.instant(`預設設定`),
        command: () => {
          this.onDittoConfigClick();
        },
      },
    ];

  }
  onDittoConfigClick() {
    throw new Error('Method not implemented.');
  }
  onDittoDetailClick() {
    throw new Error('Method not implemented.');
  }
  onDittoLastDevision() {
    throw new Error('Method not implemented.');
  }
  onDittoLastDoctor() {
    throw new Error('Method not implemented.');
  }
  onPrintClick() {
    throw new Error('Method not implemented.');
  }
  onRegisterClick() {
    throw new Error('Method not implemented.');
  }
  onCancelClick() {
    throw new Error('Method not implemented.');
  }
  onSaveClick() {
    throw new Error('Method not implemented.');
  }

  onChangeLayout() {
    throw new Error('Method not implemented.');
  }

  onShowInfoSidebar() {
    throw new Error('Method not implemented.');
  }
  onDittoClick() {
    throw new Error('Method not implemented.');
  }
  onChronicOrderClick() {
    throw new Error('Method not implemented.');
  }

  async onSideMenuClick(coding: Coding) {
    this.drugTree = await this.#opdOrderService.getMedTree(coding.code);
    this.isSlidewayCartVisible = true;
  }

  onSlidewayCartCancel() {
    this.isSlidewayCartVisible = false;
  }

  async onAddDrugOrder(codings: Coding[]) {

    if (!codings.length) return;

    const medBasics = await this.#opdOrderService.getMedBasics(codings);
    medBasics.map((x) => {
      if (x.frequency.code === '' && this.defaultDrugFrequency.code) {
        x.frequency = this.defaultDrugFrequency;
      }
    });

    const drugOrder = this.#opdOrderService.parseDrugOrders(medBasics);
    this.signalDrugOrders.update( (x) =>x.concat(drugOrder));
    this.isSlidewayCartVisible = false;
  }

}
