import { Coding } from '@his-base/datatypes';

/**
 * opd-nhi-record 所使用，傳健保資訊
 */
export interface NhiRecord {
  /** 對應的就診編號 _id  */
  medicalRecord_id: string;
  /** 卡號 */
  card: string;
  /** 健保身份 */
  nhiType: Coding;
  /** 部分負擔 */
  partType: Coding;
  /** 給付類別 */
  payType: Coding;
  /** 療程 */
  course: Coding;
  /** 優待身份 */
  genType: Coding;
  /** 重大傷病 */
  severity?: Coding;
  severityHistory: Coding[];
}

/**
 * opd-nhi-record 所使用，傳遞可更改選項
 */
export interface NhiRecordOptions {
  /** 健保身份選項 */
  nhiTypes: Coding[];
  /** 部分負擔選項 */
  partTypes: Coding[];
  /** 給付類別選項 */
  payTypes: Coding[];
  /** 療程選項 */
  courses: Coding[];
  /** 優待身份 */
  genType: Coding[];
  /** 重大傷病 */
  severityHistory: Coding[];
}

/**
 * PrimeNg SelectButton 需要傳入的資料
 */
export interface SelectButtonInfo {
  /** 每個 Button 的 class 屬性 */
  class: string;
  /** 文字 (google material 需要用此資料輸出成對應的 icon) */
  label?: string;
  /** 按鈕對應的資料 */
  value: string;
}

