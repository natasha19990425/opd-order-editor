import { Pipe, PipeTransform, inject } from "@angular/core";
import { MenuItem } from "primeng/api";
import { DrugOrderRowStatus, MenuAction } from "./opd-drug-order.enum";
import { DrugOrderEditorService } from "./drug-order-editor.service";
import { DrugOrder } from "@his-viewmodel/med-order";
import { TranslateService } from "@ngx-translate/core";

@Pipe({ name: 'getStateMenu', standalone: true })
export class GetStatusMenuPipe implements PipeTransform {

  stateMenus!: Record<DrugOrderRowStatus, MenuItem[]>;
  menuActions!: Record<MenuAction, (_id: string) => void>;

  #drugOrderService = inject(DrugOrderEditorService);
  #translate: TranslateService = inject(TranslateService);

  transform(index: number, initDrugOrders: DrugOrder[], drugOrders: DrugOrder[]) {

    this.stateMenus = {
      'none': this.getOriginalMenuItems(),
      'deleted': this.getDeleteMenuItems(),
      'new': this.getNewMenuItems(),
      'changed': this.getEditMenuItems(),
    };

    this.menuActions = {
      0: () => undefined,
      1: (_id) => this.#drugOrderService.deleteDrug(_id),
      2: (_id) => this.#drugOrderService.undoDrug(_id),
      3: (_id) => this.#drugOrderService.recoverDrug(_id)
    };

    const status = this.#drugOrderService.getStatus(index, drugOrders[index], initDrugOrders);
    return this.stateMenus[status];
  }

  /** 初始狀態的 MenuItems
   * @returns MenuItems
   */
  getOriginalMenuItems() {

    return [
      {
        icon: 'pi pi-check',
        items: [this.getDeleteOption()],
      }
    ];
  }

  /** 新增狀態的MenuItems
   * @returns MenuItems
   */
  getNewMenuItems() {

    return [
      {
        icon: 'pi pi-file',
        items: [this.getDeleteOption()],
      }
    ];
  }

  /** 編輯狀態的MenuItems
   * @returns MenuItems
   */
  getEditMenuItems() {

    return [
      {
        icon: 'pi pi-history',
        items: [this.getRecoverOption(), this.getDeleteOption()],
      }
    ];
  }

  /** 刪除狀態的MenuItems
   * @returns MenuItems
   */
  getDeleteMenuItems() {

    return [
      {
        icon: 'pi pi-times',
        items: [this.getUndoOption()],
      }
    ];
  }

  /** 刪除的選項
   * @returns
   */
  getDeleteOption() {

    return {
      label: this.#translate.instant(`刪除`),
      icon: 'pi pi-trash',
      command: () => this.menuActions[MenuAction.delete](this.#drugOrderService.selectedId),
    };
  }

  getRecoverOption() {

    return {
      label: this.#translate.instant(`復原`),
      icon: 'pi pi-history',
      command: () => this.menuActions[MenuAction.recover](this.#drugOrderService.selectedId),
    };
  }
  getUndoOption() {

    return {
      label: this.#translate.instant(`復原`),
      icon: 'pi pi-undo',
      command: () => this.menuActions[MenuAction.undo](this.#drugOrderService.selectedId),
    };
  }
}


