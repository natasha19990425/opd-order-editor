import { Injectable } from '@angular/core';
import { DrugOrder } from '@his-viewmodel/med-order';

@Injectable({
  providedIn: 'root'
})
export class OpdDrugOrderValidator {

  /** 檢查劑量是否符合規定
   * @param drugOrder 藥囑
   * @returns 是否超過限制
   */
  validateDosage(drugOrder: DrugOrder): boolean {
    // 若狀態是刪除 (是負數)，將不會檢查是否有錯，因已經刪除可忽略
    if (Number(drugOrder.status.code) < 0) {
      return false;
    }
    // 可放入需要的檢查項目，目前是限制劑量小於 100
    return drugOrder.dosage.value >= 100;
  }

  /** 總量檢查是否有小數點
   * @param drugOrder 藥囑
   * @return true: 檢核失敗 / false: 檢核成功
   */
  validateTotalQty(drugOrder: DrugOrder): boolean {

    if (drugOrder.dosage.value === 0) return true;

    const totalQty = drugOrder.total.value;
    const dosage = drugOrder.dosage.value;

    return Number.isInteger(totalQty / dosage);
  }
}
