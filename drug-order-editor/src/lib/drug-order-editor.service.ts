import { Injectable, Signal, WritableSignal, computed, effect, inject, signal } from '@angular/core';
import { Coding } from '@his-base/datatypes/dist';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { SharedService } from '@his-base/shared';
import { DrugOrder, KeyOptions, RouteOption, MedFreqTime, SearchReq, Item } from '@his-viewmodel/med-order/dist';
import { DrugOrderRowStatus, OpdDrugOrderStatus } from './opd-drug-order.enum';
import { MenuItem } from 'primeng/api';
import { OpdOrderEditorValue } from '../../../opd-order-editor/src/lib/opd-order-editor';

@Injectable({
  providedIn: 'root'
})
export class DrugOrderEditorService {

  selectedId!: string;
  #drugOrders: WritableSignal<DrugOrder[]> = signal([]);
  #initDrugOrders: DrugOrder[] = [];
  #medFreqTime!: MedFreqTime[];
  #updateTime!: Date;
  #searchReq: SearchReq;
  #freqTimes: KeyOptions = {};
  #routes: RouteOption = {};
  #options: KeyOptions = {};
  #units: KeyOptions = {};
  #useDays: KeyOptions = {};

  #jetStreamWsService = inject(JetStreamWsService);
  #sharedService = inject(SharedService);

  valueService = inject(OpdOrderEditorValue);

  unitMenu: Signal<MenuItem[]> = computed(() => this.getUnitMenu(this.valueService.options()['units']));
  useDaysMenu: Signal<MenuItem[]> = computed(() => this.getUseDaysMenu(this.valueService.options()['useDays']));

  constructor() {
    this.#searchReq = {
      branch: this.#sharedService.appPortal?.branch as string,
      date: this.#sharedService.appPortal?.orderTime as Date,
    };
  }

  /** 執行初始化操作，設定更新時間。
   * @param updateTime 要設定的更新時間
   */
  async initial(updateTime: Date) {

    this.#updateTime = updateTime;
    // this.#medFreqTime = await this.#getMedFreqTime();
  }

  /** 根據指定索引處的藥品訂單狀態判斷藥品訂單行的狀態。
   * @param index 指定的索引
   * @param drugOrder 目前的藥品訂單
   * @param initDrugOrders 初始藥品訂單列表
   * @returns 藥品訂單行的狀態
   */
  getStatus(index: number, drugOrder: DrugOrder, initDrugOrders: DrugOrder[]): DrugOrderRowStatus {
    if (drugOrder.status.code === OpdDrugOrderStatus.invalid) return DrugOrderRowStatus.deleted;
    if (index > (initDrugOrders ? initDrugOrders.length - 1 : -1)) return DrugOrderRowStatus.new;
    if (this.#isOrderChanged(index, drugOrder, initDrugOrders)) return DrugOrderRowStatus.changed;

    return DrugOrderRowStatus.none;
  }

  /** 取得使用頻率
   * @param medType
   * @return Coding[]
   */
  async getMedFreqTimes(medType: Coding): Promise<Coding[]> {

    if (!this.#freqTimes[medType.code]) {
      const searchReq = { ...this.#searchReq, keys: [medType.code] };
      const medFreqTimes = await this.#reqMedFreqTimes(searchReq);

      this.#freqTimes[medType.code] = medFreqTimes[medType.code];
    }

    return this.#freqTimes[medType.code];
  }

  /** 取得特定藥品類型的使用途徑
   * @param medType
   * @return Coding[]
   */
  async getMedRoutes(medType: Coding): Promise<Coding[]> {

    if (!this.#routes[medType.code]) {
      const searchReq = { ...this.#searchReq, keys: [medType.code] };
      const medRoutes = await this.#reqMedRoutes(searchReq);
      this.#routes[medType.code] = medRoutes[medType.code];
    }

    return this.#routes[medType.code];
  }

  getUnitMenu(options: Coding[]) {

    let unitItems = [{ label: '', command: () => this.#onUpdateUnit('') }];
    if (options && options[0]) {
      unitItems = options.map((x) => ({ label: x.code, command: () => this.#onUpdateUnit(x.code) }));

    }
    return [{ items: unitItems }];
  }

  getUseDaysMenu(options: Coding[]) {

    let unitItems = [{ label: '', command: () => this.#onUpdateUnit('') }];
    if (options && options[0]) {
      unitItems = options.map((x) => ({ label: x.code, command: () => this.#onUpdateDays(parseInt(x.code)) }));

    }
    return [{ items: unitItems }];
  }

  /** 設定藥品訂單相關資訊，包括目前藥品訂單列表和初始藥品訂單列表。
   * @param drugOrders 目前的藥品訂單列表
   * @param initDrugOrders 初始的藥品訂單列表
   */
  setDrugOrders(drugOrders: WritableSignal<DrugOrder[]>, initDrugOrders: DrugOrder[]) {

    this.#drugOrders = drugOrders;
    this.#initDrugOrders = initDrugOrders;
  }

  /** 根據指定的藥品訂單計算總量，並更新對應索引的藥品訂單的總量。
   * @param drugOrder 指定的藥品訂單
   * @param index 藥品訂單在列表中的索引
   */
  setTotalQty(drugOrder: DrugOrder, index: number) {

    if (!drugOrder) return;

    const totalQty = this.#getTotalQty(drugOrder); // 根據劑量、頻率、天數計算出總量

    if (!totalQty) return;

    this.updateDrugOrder(this.selectedId, { key: 'total', value: totalQty });

    // const order = { ...drugOrder,total: totalQty };
    // // order.total.value = totalQty;
    // this.#drugOrders.update(orders =>
    //   orders.map((x, y) => {
    //     if (y === index) return order;

    //     return x;
    //   }));
  }

  /** 更新藥品訂單的方法，根據不同目標對資料進行處理。
   * @param _id 選定的藥品訂單的 ID
   * @param target 要修改的目標，包含屬性名稱（key）和新值（value）
   * @param coding 用於 'frequency' 目標的 Coding 對象
   */
  updateDrugOrder<T>(_id: string, target?: { key: keyof DrugOrder, value: T }, coding?: Coding) {

    this.#drugOrders.update((orders) =>
      orders.map((x, y) => {

        if (x._id !== _id) return x;
        if (!target) {
          // 如果未提供目標，則表示需要還原為初始狀態
          return { ...this.#initDrugOrders[y] };
        }
        // 更新指定的屬性
        const updatedOrder = { ...x, [target.key]: target.value };
        // 如果是 'frequency' 且提供了 coding，執行相應的邏輯
        if (target.key === 'frequency' && coding) {
          this.setTotalQty(updatedOrder, y);
        }

        return updatedOrder;
      })
    );
    // 如果提供了 _id，追加更新前的訂單到原始訂單列表
    if (_id) {
      this.appendOriginalOrder(_id);
    }
  }

  /** 刪除指定 ID 的藥品訂單。
   * @param _id 要刪除的藥品訂單的 ID
   */
  deleteDrug(_id: string) {
    // 新增的資料，如果是立即依然還可以刪掉
    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    if (index >= this.#initDrugOrders.length) {
      this.#drugOrders.update((x) => x.filter((y) => y._id !== _id));
      return;
    }

    // 因有新增資料存在必須擺在第二位，已開立的資料如果立即就不能刪
    if (this.#initDrugOrders[index].frequency.code === 'STAT') return;

    const value = { code: OpdDrugOrderStatus.invalid, display: '作廢未生效' };
    this.updateDrugOrder(this.selectedId, { key: 'status', value });
  }

  /** 撤銷指定 ID 的藥品訂單。
   * @param _id 要撤銷的藥品訂單的 ID
   */
  undoDrug(_id: string) {

    const value = { code: OpdDrugOrderStatus.valid, display: '使用中' };
    this.updateDrugOrder(_id, { key: 'status', value });
  }

  /** 恢復指定 ID 的藥品訂單。
   * @param _id 要恢復的藥品訂單的 ID
   */
  recoverDrug(_id: string) {

    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    // 還原id 順序不可換
    this.updateDrugOrder(_id);

    // 把編輯後新增的那筆初始資料刪掉
    // this.#drugOrders.update(x => x.filter(y => y._id !== this.#initDrugOrders[index]._id || y.endTime.isDefaultTime()));
  }

  /** 當編輯一筆資料之後，需要將對應的初始資料加入
   * @param _id 選到的資料 id
   */
  appendOriginalOrder(_id: string) {

    // 如果超出 backup 的長度，代表是新增的，不用處理
    const index = this.#drugOrders().findIndex((x) => x._id === _id);

    if (index >= this.#initDrugOrders.length) return;

    // 有編輯的話，改變 _id 以及 updateAt
    this.updateDrugOrder(_id, { key: 'updatedAt', value: this.#updateTime });
    this.updateDrugOrder(_id, { key: '_id', value: crypto.randomUUID() });

    // 如果初始資料的 id 有找到，代表已經加過了不用再加入了 (同一筆資料編輯第二次以上)
    const foundOrder = this.#drugOrders().find((x) => x._id === this.#initDrugOrders[index]._id);

    if (foundOrder) return;

    // 設定 endTime，加入初始資料
    const order = { ...this.#initDrugOrders[index], endTime: this.#updateTime };
    this.#drugOrders.update((x) => [...x, order]);
  }

  /** 處理新增按鈕點擊事件，模擬點擊患者資訊側邊選單以觸發顯示處方箋 **/
  onAddClick() {
    const target = document.querySelector('.hpc-side-menu-prescription');
    if (!target) return;

    const clickEvent = new MouseEvent('click', {
      bubbles: true,
      cancelable: true,
      view: window,
    });
    const ariaExpanded = target.getAttribute('aria-expanded');
    if (ariaExpanded === 'true') return;

    setTimeout(() => {
      target.dispatchEvent(clickEvent);
    }, 165);
  }

  /** 處理藥品選擇事件，設定選中的藥品訂單 ID。
   * @param _id 被選中的藥品訂單的 ID
   */
  onDrugSelect(_id: string) {

    this.selectedId = _id;
  }

  /** 取得使用頻率
   *  @param medType
   */
  async #reqMedFreqTimes(searchReq: SearchReq): Promise<KeyOptions> {

    return await this.#jetStreamWsService.request('medOrder.medFreqTimes.find', searchReq);
  }

  /** 取得使用途徑
   *  @param medType
   */
  async #reqMedRoutes(searchReq: SearchReq): Promise<RouteOption> {

    return await this.#jetStreamWsService.request('medOrder.medRoutes.find', searchReq);
  }

  /** 取得單位
   *  @param medType
   */
  async #reqOption(searchReq: SearchReq): Promise<KeyOptions> {

    return await this.#jetStreamWsService.request('medOrder.option.find', searchReq);
  }

  /** 透過 frequency.code 取得該欄位 medFreqTime 進行總量計算
   * @param order 傳入 focusedDrugOrder
   * @param medFreqTime 搜尋 focusedDrugOrder 的頻率時間數值
   * @return totalQty 計算完之總量 如果分母為0會導致計算錯誤 則直接回傳 0
   * @return 如果有搜尋到對應頻率則取得該欄位頻率進行總量計算
   */
  #getTotalQty(order: DrugOrder) {

    const result = { ...order.total };
    const frequencyCode = order.frequency.code;
    const medFreqTime = this.#medFreqTime.find((item) => item.frequency.code === frequencyCode);

    if (!medFreqTime) return result;

    if (medFreqTime.denominator === 0) return result;

    const dosage = order.dosage.value;
    const usedNum = medFreqTime.timePoints.length;
    const { molecular, denominator } = medFreqTime;

    const totalQty = (molecular / denominator) * usedNum * dosage * order.usedDays;

    result.value = totalQty;
    if (totalQty % 1 === 0) return result;

    result.value = parseFloat(totalQty.toFixed(2));
    return result;
  }

  /** 更新藥品訂單的單位信息並重新計算總量
   * @param unit 要更新的單位
   */
  #onUpdateUnit(unit: string) {

    const order = this.#drugOrders().find((x) => x._id === this.selectedId);
    if (!order) return;

    let value = { ...order.dosage, code: unit, unit: unit };
    this.updateDrugOrder(this.selectedId, { key: 'dosage', value });

    value = { ...order.total, code: unit, unit: unit };
    this.updateDrugOrder(this.selectedId, { key: 'total', value });

    this.appendOriginalOrder(this.selectedId);
  }


  /** 更新藥品訂單的使用天數，並重新計算總量
   * @param value 要更新的使用天數值
   */
  #onUpdateDays(value: number) {

    this.updateDrugOrder(this.selectedId, { key: 'usedDays', value });
    const order = this.#drugOrders().find((x) => x._id === this.selectedId);

    if (!order) return;
    const totalQty = this.#getTotalQty(order); // 根據劑量、頻率、天數計算出總量
    if (!totalQty) return;

    this.updateDrugOrder(this.selectedId, { key: 'total', value: totalQty });
    this.appendOriginalOrder(this.selectedId);
  }

  /** 檢查指定索引處的藥品訂單是否有變更
   * @param index 指定的索引
   * @param drugOrder 目前的藥品訂單
   * @param initDrugOrders 初始藥品訂單列表
   * @returns 如果訂單有變更，則返回 true；否則返回 false。
   */
  #isOrderChanged(index: number, drugOrder: DrugOrder, initDrugOrders: DrugOrder[]) {
    if (!initDrugOrders[index]) return;

    const excludeProperty = (order: DrugOrder) => {
      const { status, ...newCopy } = order;
      return newCopy;
    };

    const backupWithoutStatus = excludeProperty(initDrugOrders[index]);
    const rowDrugOrderWithoutStatus = excludeProperty(drugOrder);

    return JSON.stringify(backupWithoutStatus) !== JSON.stringify(rowDrugOrderWithoutStatus);
  }

  // parseMenuItem(codings: Coding[]): MenuItem[] {
  //   if (!codings.length) return [];
  //   return codings.map((x: Coding) => {
  //     const { code, display } = x;
  //     const label = display;
  //     const command = this.#onUpdateUnit(code);
  //     return {label, command};
  //   });
  // }

}
