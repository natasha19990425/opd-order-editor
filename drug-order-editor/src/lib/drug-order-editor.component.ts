import { Coding } from '@his-base/datatypes';
import { Component, EventEmitter, Input, Output, WritableSignal, effect, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { MegaMenuModule } from 'primeng/megamenu';
import { MenuItem } from 'primeng/api';
import { MenubarModule } from 'primeng/menubar';
import { ScrollPanelModule } from 'primeng/scrollpanel';
import { MenuModule } from 'primeng/menu';
import { InputNumberModule } from 'primeng/inputnumber';
import { FormsModule } from '@angular/forms';
import { CheckboxModule } from 'primeng/checkbox';
import { GetRowCssPipe } from './get-row-css.pipe';
import { GetStatusMenuPipe } from './get-state-menu.pipe';
import { FilterOrdersPipe } from './filter-orders.pipe';
import { DrugOrder } from '@his-viewmodel/med-order';
import { Item, ItemsDialogComponent } from '@his-directive/items-dialog/dist/items-dialog';
import { DrugOrderEditorService } from './drug-order-editor.service';
import { TranslateModule, TranslateService } from '@ngx-translate/core';
import { OpdDrugOrderValidator } from './opd-drug-order.validator';
import { DrugOrderRowStatus } from './opd-drug-order.enum';
const primeGroup = [TableModule, ButtonModule, CheckboxModule, FormsModule, DialogModule, MegaMenuModule, MenubarModule, ScrollPanelModule, MenuModule, InputNumberModule,
  GetRowCssPipe, GetStatusMenuPipe, FilterOrdersPipe, ItemsDialogComponent,];
@Component({
  selector: 'his-drug-order-editor',
  standalone: true,
  imports: [CommonModule, TranslateModule, primeGroup],
  templateUrl: './drug-order-editor.component.html',
  styleUrls: ['./drug-order-editor.component.scss'],
})
export class DrugOrderEditorComponent {

  /** 藥品處方 **/
  @Input() drugOrders!: WritableSignal<DrugOrder[]>;

  /** 藥品處方備份 **/
  @Input() set initDrugOrders(value: DrugOrder[]) {

    let _initDrugOrders: DrugOrder[] = [];
    if (!value) return;
    _initDrugOrders = value;
    this.#drugOrderEditorService.setDrugOrders(this.drugOrders, _initDrugOrders);
  }

  /** table 可視畫面高度 **/
  @Input() scrollHeight!: string;

  /** 更新時間：抓進入畫面的時間 **/
  @Input() updateTime!: Date;

  /** 傳遞出要打開 sidemenu 的要求 **/
  @Output() sideMenuVisible = new EventEmitter();

  isSelectDialogVisible = false;
  isEnjoinVisible = false;
  selectedEnjoin = '';
  selectItems!: Item[];
  selectItemTitle = '';
  unitItems: MenuItem[] = [];
  useDayItems!: MenuItem[];

  #selectedType = '';
  #selectedId!: string;
  #routeItems: Item[] = [];
  #frequencyItems: Item[] = [];

  #drugOrderValidator: OpdDrugOrderValidator = inject(OpdDrugOrderValidator);
  #drugOrderEditorService: DrugOrderEditorService = inject(DrugOrderEditorService);
  #translate: TranslateService = inject(TranslateService);

  unitMenu = this.#drugOrderEditorService.unitMenu;
  useDaysMenu = this.#drugOrderEditorService.useDaysMenu;

  updateItems = effect(() => {
    this.unitItems = this.unitMenu();
    this.useDayItems = this.useDaysMenu();

  });
  ngOnInit() {

    this.#drugOrderEditorService.initial(this.updateTime);
  }

  isDosageInvalid(drugOrder: DrugOrder): boolean {
    return this.#drugOrderValidator.validateDosage(drugOrder);
  }

  isFrequencyInvalid(drugOrder: DrugOrder): boolean {
    return !this.#drugOrderValidator.validateTotalQty(drugOrder);
  }

  /** 設置總量到 focusedOrder.total.value
   * @param {DrugOrder} drugOrder
   * @param {number} index
   */
  updateTotalQty(drugOrder: DrugOrder, index: number) {

    this.#drugOrderEditorService.setTotalQty(drugOrder, index);
  }

  /** 更新藥品訂單的劑量，計算總量並追加更新前的訂單到原始訂單列表
   * @param drugOrder 要更新的藥品訂單
   * @param index 藥品訂單在列表中的索引
   */
  updateDosage(drugOrder: DrugOrder, index: number) {

    this.updateTotalQty(drugOrder, index);
    this.#drugOrderEditorService.appendOriginalOrder(drugOrder._id);
  }

  /** 處理項目選擇事件，將選中的藥品訂單 ID 傳遞給藥品訂單編輯服務處理藥品選擇。
   * @param _id 被選中的藥品訂單的 ID
   */
  onItemSelect(_id: string) {

    this.#drugOrderEditorService.onDrugSelect(_id);
  }

  /** 處理項目變更事件，將選擇的 Coding 傳遞給藥品訂單編輯服務處理數值變更。
   * @param coding
   */
  onItemChange(coding: Coding): void {

    this.#drugOrderEditorService.updateDrugOrder(this.#selectedId, { key: this.#selectedType as keyof DrugOrder, value: coding }, coding);
  }

  /** 顯示選單
   * @param {string} type
   * @param {string} _id
   * @param {Coding} medType
   * @memberof DrugOrderEditorComponent
   */
  async onShowDialog(type: string, _id: string, medType: Coding) {

    this.#selectedId = _id;
    this.#selectedType = type;

    if (type === 'route') {
      this.#routeItems = await this.#drugOrderEditorService.getMedRoutes(new Coding({ code: medType.code }));
      this.selectItems = this.#routeItems;
      this.selectItemTitle = this.#translate.instant(`請輸入途徑`);
    }

    if (type === 'frequency') {
      this.#frequencyItems = await this.#drugOrderEditorService.getMedFreqTimes(new Coding({ code: medType.code }));
      this.selectItems = this.#frequencyItems;
      this.selectItemTitle = this.#translate.instant(`請輸入頻率`);
    }

    this.isSelectDialogVisible = !this.isSelectDialogVisible;
  }

  /** 處理建議編輯事件，設定選中的藥品訂單 ID，切換建議框的可見狀態，並取得選中訂單的建議。
   * @param _id 被選中的藥品訂單的 ID
   */
  onAdviseEdit(_id: string) {

    this.#selectedId = _id;
    this.isEnjoinVisible = !this.isEnjoinVisible;
    const selectedOrder = this.drugOrders().find((x) => x._id === this.#selectedId);
    if (!selectedOrder) return;
    this.selectedEnjoin = selectedOrder.advise;
  }

  /** 處理建議取消事件，隱藏建議框 **/
  onAdviseCancel() {

    this.isEnjoinVisible = false;
  }

  /** 處理建議確認事件，更新指定 ID 的藥品訂單的建議並追加到原始訂單列表，隱藏建議框 **/
  onAdviseConfirm() {

    this.#drugOrderEditorService.updateDrugOrder(this.#selectedId, { key: 'advise', value: this.selectedEnjoin });
    this.#drugOrderEditorService.appendOriginalOrder(this.#selectedId);
    this.isEnjoinVisible = false;
  }

  /** 處理新增按鈕點擊事件，發送側邊選單可見的事件並觸發對應服務的新增按鈕點擊操作 **/
  onAddClick() {
    this.sideMenuVisible.emit(true);
    this.#drugOrderEditorService.onAddClick();
  }

}
