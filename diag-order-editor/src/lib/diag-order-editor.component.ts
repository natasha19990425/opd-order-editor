import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'his-diag-order-editor',
  standalone: true,
  imports: [CommonModule],
  templateUrl: 'diag-order-editor.component.html',
  styleUrls: ['./diag-order-editor.component.scss']
})
export class DiagOrderEditorComponent {

}
