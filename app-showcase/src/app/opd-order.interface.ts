import { Coding } from '@his-base/datatypes';
import { DrugOrder, ExamOrder, DiagOrder } from '@his-viewmodel/med-order';

/**
 * opd-nhi-record 所使用，傳健保資訊
 */
export interface NhiRecord {
  /** 對應的就診編號 _id  */
  medicalRecord_id: string;
  /** 卡號 */
  card: string;
  /** 健保身份 */
  nhiType: Coding;
  /** 部分負擔 */
  partType: Coding;
  /** 給付類別 */
  payType: Coding;
  /** 療程 */
  course: Coding;
  /** 優待身份 */
  genType: Coding;
  /** 重大傷病 */
  severity? : Coding;
  severityHistory: Coding[];
}

/**
 * opd-nhi-record 所使用，傳遞可更改選項
 */
export interface NhiRecordOptions {
  /** 健保身份選項 */
  nhiTypes: Coding[];
  /** 部分負擔選項 */
  partTypes: Coding[];
  /** 給付類別選項 */
  payTypes: Coding[];
  /** 療程選項 */
  courses: Coding[];
  /** 優待身份 */
  genType: Coding[];
  /** 重大傷病 */
  severityHistory: Coding[];
}

/**
 * 配合 病人清單取名為 visitRecord
 * MedicalRecord 改名為 VisitDetail
 */
export interface VisitDetail {
  /** 對應的 VisitRecord _id
   * 若沒有資料，則代表是尚未存檔過的紀錄
   */
  visitRecord_id?: string;
  /** 看診日期 */
  visitDate: Date;
  /** 看診科別 */
  devision: Coding;
  /** 醫師 */
  doctor: Coding;
  /** 健保相關項目 */
  // nhiRecord?: MedRecord;
  subjective: string;
  objective: string;
  assessment: string;
  plan: string;
  diagOrders: DiagOrder[];
  drugOrders: DrugOrder[];
  examOrders: ExamOrder[];
}

/**
 * PrimeNg SelectButton 需要傳入的資料
 */
export interface SelectButtonInfo {
  /** 每個 Button 的 class 屬性 */
  class: string;
  /** 文字 (google material 需要用此資料輸出成對應的 icon) */
  label?: string;
  /** 按鈕對應的資料 */
  value: string;
}

