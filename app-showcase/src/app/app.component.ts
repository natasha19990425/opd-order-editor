import { Component, OnDestroy, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterOutlet } from '@angular/router';
import { OpdOrderEditorComponent } from '../../../opd-order-editor/src/public-api';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { INIT_CONNECT } from './app.config';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [CommonModule, RouterOutlet, OpdOrderEditorComponent],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  #jetStreamWsService = inject(JetStreamWsService);
  // 連線網址
  #url = 'ws://10.251.42.49:8080';
  #user = '';
  #password = '';

  connectState = inject(INIT_CONNECT);

  async ngOnDestroy() {
    await this.#jetStreamWsService.drain();

  }
  async ngOnInit() {
    await this.#jetStreamWsService.connect(this.#url, this.#user, this.#password);
    this.connectState.set(true);
  }

}
