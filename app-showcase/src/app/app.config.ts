import { ApplicationConfig, InjectionToken, WritableSignal, importProvidersFrom, signal } from '@angular/core';
import { provideRouter } from '@angular/router';
import { provideAnimations } from '@angular/platform-browser/animations';

import { routes } from './app.routes';
import { HttpClient, provideHttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import "@angular/compiler";
// import '@angular/localize/init';
import { JetStreamWsService } from '@his-base/jetstream-ws';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, '../../assets/i18n/', '.json');
}
export const INIT_CONNECT = new InjectionToken<WritableSignal<boolean>>('');
export const appConfig: ApplicationConfig = {

  providers: [
    provideRouter(routes),
    provideAnimations(),
    provideHttpClient(),
    importProvidersFrom(TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      },
    })),
    {
      provide: JetStreamWsService,
      useValue: new JetStreamWsService({
        // stream name
        name: 'HEPIUSCARE',
      }),
    },
    {
      provide: INIT_CONNECT,
      useValue: signal(false)
    }
  ],
};
