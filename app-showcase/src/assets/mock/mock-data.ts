
import { SoapNote, DrugOrder, ExamOrder, DiagOrder, OpdMedOrder, MedRoute, MedFreqTime } from '@his-viewmodel/med-order';
import { MedRecord } from '@his-viewmodel/patient-info';
import { NhiRecord, NhiRecordOptions, VisitDetail } from './../../app/opd-order.interface';
import { DiagItem } from '../../app/diagnosis.interface';
import { Item } from '@his-directive/multi-slideway/dist/multi-slideway';
import { Registration } from '@his-viewmodel/patient-info';
import { MenuTitle } from '@his-directive/side-menu/dist/side-menu';
import { MenuItem as CartMenuItem, Group as CartGroup, } from '@his-directive/cart-list/dist/cart-list';
import { Coding } from '@his-base/datatypes';
import '@his-base/date-extention';

export const mockRoute: MedRoute[] = [
  {
    "_id": "b6838a37-3182-4b59-860e-a183e12f5fff",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "AD", "display": "右耳" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "1e8c7c81-0563-4d8e-84a3-f19393b9b137",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "AS", "display": "左耳" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "6b7f0c02-54a9-4886-b6a4-886c8a5bcff5",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "AU", "display": "雙耳" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "123a943d-9425-4904-8eea-61b5d8676a9b",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "ET", "display": "氣內切(Endotracheal)" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "26d400df-ce9f-49c4-89df-bc3d06ebf12a",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "GAR", "display": "漱口用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "df39d1ce-44c7-4e27-ad0c-bede14dec643",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "HD", "display": "皮下灌注" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "1bb2b2ab-db7b-4cbc-b747-88f17b70de68",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IA", "display": "動脈注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "29f9ab22-55cf-4831-b832-5c25de57efe1",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "ID", "display": "皮內注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "aeb60ecb-9534-42cf-9e49-55ba2a2faf89",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IE", "display": "脊隨硬膜內注" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "ab4e098f-025a-4e74-8538-7645ce81c2cb",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IM", "display": "肌肉注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "6d236657-eaaf-46e6-8089-a42371e26b6f",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IMP", "display": "植入" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "9085c6a5-c876-4163-9581-6acada03ef87",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "INHL", "display": "吸入" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "e1a4042b-51cb-4f47-8bdd-f0de3b38f966",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IS", "display": "滑膜內注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "373ca371-548e-4041-b61b-e4b015ee58c9",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IT", "display": "椎骨內注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "240accbb-bbdb-4b03-b43d-581073642a7f",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IVA", "display": "靜脈添加" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "0617fdff-37cd-49d3-a752-2dd791b9d632",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IVI", "display": "玻璃體內注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "a541f103-14e9-47c5-af99-a71e1311be34",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IVP", "display": "靜脈注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "fe9f44c5-4454-489f-842a-96b7eef71bea",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "LA", "display": "局部外用塗擦" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "e850bc0d-db1c-4e83-8542-5968f24e0036",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "LI", "display": "局部注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "51c9d4e8-abaf-4bd1-a7f3-7b3be10a21ab",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "NA", "display": "鼻用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "7cc138e5-22f4-44b8-aed9-6bdca4475a39",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "O", "display": "口腔用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "d32d59bd-a831-4ef3-8234-f048d17d5de3",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "OD", "display": "右眼" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "4fadcbdd-0b00-4d89-9a56-6d3da50f21e1",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "OS", "display": "左眼" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "c8d2512a-41db-4215-bcac-d2a37f338e6a",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "OU", "display": "雙眼" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "44c7a916-88d7-4b4e-b37e-ec8a3af4cfdd",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "PO", "display": "口服" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "e30cbb00-2c0d-49bd-8501-27bf378f2d6b",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SC", "display": "皮下注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "61efc726-eb48-4b53-9853-92717c94892d",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SCI", "display": "結膜下注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "8bab1271-346e-45b4-adc7-d538601aed87",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SKIN", "display": "皮膚用" },
    "child": [{
      "_id": "9aff8b11-8d85-45c3-9643-563b6f4bdb94",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "臉", "display": "臉" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "bd79d3ab-48e4-4be9-924a-02ad4b84d53a",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "頭", "display": "頭" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "10a93b54-0310-4384-be0b-72ee5fe88c50",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "胸", "display": "胸" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "548cd7e0-b7b1-4083-9bc5-a39fb5dd9886",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "腹", "display": "腹" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "ab9cc56e-d2a8-4a35-aa6a-2c2020bca62b",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "背", "display": "背" },
      "child": [{
        "_id": "a857d3f9-0b20-42c4-b543-0374c0801c41",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "右側", "display": "右側" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "8db1a35d-fa67-4bc7-97dd-a21e00c852d8",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "左側", "display": "左側" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "9cbdf78d-bd2b-4067-9650-dbe5d9abe45c",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "雙側", "display": "雙側" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "566c18a6-55db-4b4c-98ea-31e0effe1ab2",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "上", "display": "上" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "4f08d613-b637-4a20-8c07-e95bbe271c7a",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "下", "display": "下" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "66233de4-c3f9-4d8b-8839-d15c0df380ff",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "前", "display": "前" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }, {
        "_id": "f62a60b4-d762-4b73-ab70-14d2e265d1e4",
        "medType": { "code": "route", "display": "route" },
        "route": { "code": "後", "display": "後" },
        "child": [],
        "startTime": new Date("2023-10-26 15:26:05"),
        "endTime": new Date("2999-12-31 15:59:00"),
        "updatedBy": { "code": "0016", "display": "0016" },
        "updatedAt": new Date("2023-10-26 15:26:05")
      }],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "ed9b33cf-3ff4-4f08-8bc6-65647b76d35d",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "軀幹", "display": "軀幹" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "61f0e72b-833f-479e-b22e-2c5409573d89",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "大腿", "display": "大腿" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "5e9ffb27-695f-4757-aa1f-70c3b5e4cf55",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "小腿", "display": "小腿" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "b29eb96f-1fce-4659-98f1-d205d3d6ae83",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "足", "display": "足" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "8ba22547-998f-4862-a4ff-85f23ac4f897",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SL", "display": "含於舌下" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "de051db0-553f-4e57-8149-6595c33058d3",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SUPP", "display": "肛門用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "b37bddab-7eca-4e6d-87d1-e167791a3226",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "TOPI", "display": "塗擦患部" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "74b2dede-c13f-4bee-8426-f9115ec75c85",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "TPN", "display": "全靜脈營養劑" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "4d693f48-55b4-4258-9fcc-9134b633c1c3",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "VAG", "display": "陰道用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "09cf92bd-fb53-42e9-9f4f-9e7a0343a707",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "EXT", "display": "外用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "6a993c4b-8f85-47f4-acce-ddfe660acc4b",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IVD", "display": "大量點滴注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "1dba6d1b-5668-493c-8daf-6ca2d55260d5",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "CHEM", "display": "化學治療" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "3e7ddd51-c6c3-4704-b459-eee09455d7f4",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "RECT", "display": "肛門用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "59c32236-3209-4f22-9dd7-95201d523e02",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "SPI", "display": "脊隨" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "12810a1c-d408-4b93-877d-b5893cc62dc9",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IRRI", "display": "沖洗" },
    "child": [{
      "_id": "c8055204-66d0-4eda-8ae7-a660f9289a2d",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "Wound", "display": "Wound" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "650deebb-77c3-4066-bed9-7194a3716326",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "NG", "display": "NG" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }, {
      "_id": "ed4ca827-c98d-4f1a-860f-4c889bd8ab8a",
      "medType": { "code": "route", "display": "route" },
      "route": { "code": "Bladder", "display": "Bladder" },
      "child": [],
      "startTime": new Date("2023-10-26 15:26:05"),
      "endTime": new Date("2999-12-31 15:59:00"),
      "updatedBy": { "code": "0016", "display": "0016" },
      "updatedAt": new Date("2023-10-26 15:26:05")
    }],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "b83ef646-2a66-4fe5-8e85-5257b95adff6",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "IP", "display": "腹腔加藥" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "0073c851-3005-41a8-aa68-864e3683ddb4",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "A", "display": "靜脈添加" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "f5da8735-8ba3-447a-b020-fd9517eb4468",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "D", "display": "靜脈點滴注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "87961443-7ac2-4280-bae6-9d43ccd5c897",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "H", "display": "吸入" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "7bcc8139-d40e-44f2-a236-7c4dd48e1a00",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "M", "display": "肌肉注射" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "ce6299e4-1f53-4904-a3fe-72b24bb24ec6",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "T", "display": "塗抹患部" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }, {
    "_id": "84753b66-a625-4a1c-b5b9-0ea2a2226898",
    "medType": { "code": "route", "display": "route" },
    "route": { "code": "U", "display": "肛門用" },
    "child": [],
    "startTime": new Date("2023-10-26 15:26:05"),
    "endTime": new Date("2999-12-31 15:59:00"),
    "updatedBy": { "code": "0016", "display": "0016" },
    "updatedAt": new Date("2023-10-26 15:26:05")
  }
];

export const mockFreqTime: MedFreqTime[] = [
  {
    "_id": "9bfb9710-a13d-4575-835c-350cb0ed0f5f",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QW135", "display": "每星期一三五使用" },
    "molecular": 3,
    "denominator": 7,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "3750a2cd-e716-4462-90cf-1475f22a7565",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QW246", "display": "每星期二四六使用" },
    "molecular": 3,
    "denominator": 7,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "3ba9d976-af1f-449d-a57d-4658222ad3b6",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q1D", "display": "每1日一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "f346fcdb-4a03-49e2-b986-8d6ed5009aae",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q6D", "display": "每6日一次" },
    "molecular": 1,
    "denominator": 6,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "4d05910d-9e8a-4ce8-86cd-69624c26429f",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q2W", "display": "每2星期一次" },
    "molecular": 1,
    "denominator": 14,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "d64c5c42-2f28-493d-a4b8-24c22a796cb3",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q3M", "display": "每3月一次" },
    "molecular": 1,
    "denominator": 90,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "32c500d5-3d7f-4494-95af-d2db1cf9a385",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QW", "display": "每週一次" },
    "molecular": 1,
    "denominator": 7,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "87702fae-ac13-42b6-8e17-9964e970728b",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "BIW", "display": "每週二次" },
    "molecular": 2,
    "denominator": 7,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "b4230868-0481-4de0-8f89-1b27311ad4ec",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "TIW", "display": "每週三次" },
    "molecular": 3,
    "denominator": 7,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "0d7c1af8-d057-4adc-85c1-d5e32f9e1ca7",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q6H", "display": "每6小時使用一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [6, 12, 18, 24],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "1adbf4e0-41bc-4883-be56-d80cd46b0e65",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "Q20MN", "display": "每20分鐘使用一次" },
    "molecular": 24,
    "denominator": 1,
    "timePoints": [20, 40, 60],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "cec5a146-b13e-47f3-8481-191c3bdbdacf",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QD", "display": "每日一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "07e72711-4d0c-42fd-b172-451be013da46",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QDAM", "display": "每日一次上午使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "eab75e1a-3faa-4527-b7d5-0d2dc9349024",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QDPM", "display": "每日一次下午使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [13],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "1c8fe4c0-c886-414f-84f8-cd72ae775c8f",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QDHS", "display": "每日一次睡前使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "9cf36078-91e9-4db7-942d-72929b7c922b",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QN", "display": "每晚使用一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [19],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "2b3f727b-80f5-4351-bfa6-585ee1426509",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "BID", "display": "每日二次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "143e648d-797a-4063-81bb-069105c4fa6c",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QAM&HS", "display": "上午使用一次且睡前一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "b6b23f27-91e3-456d-a6d5-c81f60beeafa",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QPM&HS", "display": "下午使用一次且睡前一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [13, 21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "c9f7910d-7bcf-49b2-8a76-b84695eec8d0",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QAM&PM", "display": "每日上、下午各使用一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 17],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "e3866060-9095-40d2-93db-77100a24b016",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QAM&PM", "display": "每日三次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 17],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "8cc518b7-72f1-43c9-b67e-76e96242d599",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "BID&HS", "display": "每日二次且睡前一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "b7e22561-7c80-4a77-acc5-4ba052605dcb",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QID", "display": "每日四次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 17, 21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "999b5532-e0ed-4c5c-8cfb-c8b0188c17ee",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "QID", "display": "每日三次且睡前一次" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 17, 21],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "137e0c9a-904a-4ad0-bf25-1fb3f1366c63",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "AC", "display": "飯前" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [7, 12, 17],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "d21e38f4-9015-4487-8ce8-ebababca23a7",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "AC1H", "display": "飯前1小時使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [6, 11, 16],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "a629a5db-d223-4756-818c-0dd087e54945",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "AC10M", "display": "飯前10分鐘使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [7, 12, 17],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "15f0d1a6-e955-4dc5-9728-69f0ddfbec93",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "PC", "display": "飯後" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 18],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "075a9051-e08a-4085-99f2-ca943ad6c982",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "PC1H", "display": "飯後1小時使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 18],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "3a946a52-062d-487f-a830-819de0eda674",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "PC20M", "display": "飯後20分鐘使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9, 13, 18],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "e0649130-a0dd-4d1c-808a-7134ae2d45de",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "STAT", "display": "立即使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "36f457c8-d50c-4fdd-acc0-bd497bb93486",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "ASORDER", "display": "依照醫生指示使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }, {
    "_id": "f7a52fbd-037b-4cfd-8ede-c85d2488eadf",
    "medType": { "code": "drug", "display": "藥品" },
    "visitType": "OEA",
    "frequency": { "code": "prn", "display": "需要時使用" },
    "molecular": 1,
    "denominator": 1,
    "timePoints": [9],
    "startTime": new Date("2022-01-01 00:00"),
    "endTime": new Date("2999-12-31 23:59"),
    "updatedBy": { "code": "system", "display": "系統產生" },
    "updatedAt": new Date("2023-08-01 00:00")
  }
];

/** opd-nhi-record 所使用選項 假資料傳入 */
export const mockSelectOptions: NhiRecordOptions = {
  nhiTypes: [
    { code: '1', display: '自費' },
    { code: '2', display: '一般健保' },
    { code: '3', display: '農民健保' },
    { code: '4', display: '公教健保' },
    { code: '5', display: '軍人健保' },
    { code: '6', display: '依親健保' },
    { code: '7', display: '退休軍人健保' },
    { code: '8', display: '警察健保' },
    { code: '9', display: '特種健保' },
    { code: '10', display: '其他特殊健保類別' },
  ],
  partTypes: [
    { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    { code: 'A12', display: '一般門診', system: '西醫-醫學中心' },
    {
      code: 'A13',
      display: '一般門診-持殘障手冊或參加試辦計畫補助者',
      system: '西醫-醫學中心',
    },
    { code: 'A20', display: '藥品、復健或高利用率者', system: '西醫-醫學中心' },
    {
      code: 'A23',
      display: '藥品、復健、或高利用率者-持殘障手冊',
      system: '西醫-醫學中心',
    },
    {
      code: 'A30',
      display: '轉診（轉入之院所適用、藥品或復健',
      system: '西醫-醫學中心',
    },
    {
      code: 'A40',
      display: '住院出院或門診手術 7 日內回診、藥品或復健',
      system: '西醫-醫學中心',
    },
  ],
  payTypes: [
    { code: '1', display: '職業傷害' },
    { code: '2', display: '職業病' },
    { code: '3', display: '普通傷害' },
    { code: '4', display: '普通疾病' },
    { code: '5', display: '產前檢查' },
    { code: '6', display: '自然生產' },
    { code: '7', display: '剖腹生產' },
    { code: '9', display: '呼吸照護' },
    { code: 'Z', display: '其他' },
  ],
  courses: [
    { code: '0', display: '非療程' },
    { code: '1', display: '療程開始' },
    { code: '2', display: '療程中' },
    { code: '3', display: '療程結束' },
  ],
  genType: [
    { code: '0', display: '百歲人瑞' },
    { code: '1', display: '學院學生' },
    { code: '2', display: '二分局員工' },
    { code: '3', display: '洗腎免掛' },
    { code: '4', display: '學院A員眷' },
  ],
  severityHistory: [{ code: '不適用', display: '不適用' },],

};

export const mockDrugSubstitution = [
  {
    code: {
      code: 'TMYCOPH2',
      display: 'TMYCOPH2',
    },
    invCode: 'MYCOphenolate Mofetil 150mg/Cap',
    startTime: new Date('2019-04-14'),
    endTime: new Date('2031-12-31')
  }, {
    code: {
      code: 'TMYCOPH3',
      display: 'TMYCOPH3',
    },
    invCode: 'MYCOphenolate Mofetil 250mg/Cap',
    startTime: new Date('2019-04-14'),
    endTime: new Date('2031-12-31')
  }, {
    code: {
      code: 'TMYCOPH4',
      display: 'TMYCOPH4',
    },
    invCode: 'MYCOphenolate Mofetil 350mg/Cap',
    startTime: new Date('2019-04-14'),
    endTime: new Date('2031-12-31')
  }
];

export const mockVitalSign =
{
  _id: '',
  chartNo: '',
  nhiCardNo: '1234',
  systolic: {
    code: 'mmHg',
    value: 120,
    unit: 'mmHg',
  },
  diastolic: {
    code: 'mmHg',
    value: 80,
    unit: 'mmHg',
  },
  pulse: {
    code: '/min',
    value: 92,
    unit: '/min',
  },
  height: {
    code: 'cm',
    value: 172,
    unit: 'cm',
  },
  weight: {
    code: 'kg',
    value: 68.35,
    unit: 'kg',
  },
  bmi: {
    code: 'mmHg',
    value: 23.1,
    unit: 'mmHg',
  },
  bodyTemperature: {
    code: '°C',
    value: 37,
    unit: '°C',
  },
  waistCircumference: {
    code: 'cm',
    value: 65,
    unit: 'cm',
  },
  headCircumference: {
    code: 'cm',
    value: 52,
    unit: 'cm',
  },
  updatedBy: {
    code: '033',
    display: '林醫師'
  },
  updatedAt: new Date(),
};
// TODO: 改為 DrugRemaining 型態
export const mockDrugRemainings = [
  {
    name: 'B群',
    day: 20,
  },
  {
    name: '止痛藥',
    day: 24,
  },
];
export const mockMenuTitles: MenuTitle[] = [
  {
    icon: 'pi pi-book',
    title: { code: 'diagnosisIssued', display: '診斷開立' },
    child: [
      new MenuTitle({
        title: { code: 'keywordSearch', display: '關鍵字查詢' },
      }),
      new MenuTitle({
        title: { code: 'specialties', display: '專科診斷' },
      }),
      new MenuTitle({
        title: { code: 'icd', display: 'ICD分類' },
      }),
      new MenuTitle({
        title: { code: 'ai', display: 'AI推薦診斷' },
      }),
      new MenuTitle({
        title: { code: 'physicianNotes', display: '個人診斷' },
      }),
    ],
  },
  {
    icon: 'pi pi-book',
    title: { code: 'prescription', display: '開立處方' },
    child: [
      new MenuTitle({
        title: { code: 'inspection', display: '檢驗檢查' },
      }),
      new MenuTitle({
        title: { code: 'radiation', display: '放射檢查' },
      }),
      new MenuTitle({
        title: { code: 'internalMedicine', display: '內科檢查' },
      }),
      new MenuTitle({
        title: { code: 'surgical', display: '外科檢查' },
      }),
      new MenuTitle({
        title: { code: 'obstetrics', display: '婦產檢查' },
      }),
      new MenuTitle({
        title: { code: 'child', display: '兒童檢查' },
      }),
      new MenuTitle({
        title: { code: 'other', display: '其他檢查' },
      }),
      new MenuTitle({
        title: { code: 'treat', display: '治療及處置' },
      }),
      new MenuTitle({
        title: { code: '1000000', display: '院內用藥' },
      }),
      new MenuTitle({
        title: { code: 'own', display: '自備用藥' },
      }),
    ],
  },
];

export const mockMedicalRecords: MedRecord[] = [
  {
    _id: '4283f9fb-7b75-455a-8400-84a4f44cfa8b',
    // nhiCardNo: '5678',
    nhiType: { code: '2', display: '一般健保' },
    partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    payType: { code: '1', display: '職業傷害' },
    therapyPlan: { code: '0', display: '非療程' },
    genType: { code: '1', display: '學院學生' },
    severity: { code: '不適用', display: '不適用' },
    severityHistory: [
      { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
      { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
    ],
    // hospital: new Coding(),
    chartNo: '0038842103',
    idNo: 'B123456789',
    visitDate: new Date('2023-04-11'),
    visitSeqNo: '',
    visitSource: new Coding(),
    isFirstVisit: false,
    visitType: new Coding(),
    doctor: new Coding(),
    division: new Coding(),
    // roomNo: '305診',
    branch: new Coding(),
    room: '',
    seqNo: '4號',
    checkInTime: new Date(),
    plannedStartTime: new Date(),
    plannedEndTime: new Date('2999-12-31 23:59'),
    dischargeDisposition: new Coding(),
    currentStatus: new Coding(),
    caseType: new Coding(),
    isSensitive: false,
    chargeStatus: new Coding(),
    medCharge_id: '',
    remark: '',
    updatedBy: new Coding(),
    updatedAt: new Date(),
    visitTimeSlot: { code: '2', display: '下午' },
    triage: new Coding,
  },
  {
    _id: '6bac6a6f-6666-66d6-66af-ea66f6bb666b',
    // nhiCardNo: '1234',
    nhiType: { code: '4', display: '公教健保' },
    partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    payType: { code: '1', display: '職業傷害' },
    therapyPlan: { code: '0', display: '非療程' },
    genType: { code: '1', display: '學院學生' },
    severity: { code: '不適用', display: '不適用' },
    severityHistory: [
      { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
      { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
    ],
    // hospital: new Coding(),
    chartNo: '0046161646',
    idNo: 'L226466686',
    visitDate: new Date('2016-06-06'),
    visitSeqNo: '',
    visitSource: new Coding(),
    isFirstVisit: false,
    visitType: new Coding(),
    doctor: new Coding(),
    division: new Coding(),
    // roomNo: '006診',
    branch: new Coding(),
    room: '',
    seqNo: '66號',
    checkInTime: new Date(),
    plannedStartTime: new Date(),
    plannedEndTime: new Date('2999-12-31 23:59'),
    dischargeDisposition: new Coding(),
    currentStatus: new Coding(),
    caseType: new Coding(),
    isSensitive: false,
    chargeStatus: new Coding(),
    medCharge_id: '',
    remark: '',
    updatedBy: new Coding(),
    updatedAt: new Date(),
    visitTimeSlot: { code: '1', display: '早上' },
    triage: new Coding,
  },
  {
    _id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
    // nhiCardNo: '1234',
    nhiType: { code: '2', display: '一般健保' },
    partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    payType: { code: '1', display: '職業傷害' },
    therapyPlan: { code: '0', display: '非療程' },
    genType: { code: '1', display: '學院學生' },
    severity: { code: '不適用', display: '不適用' },
    severityHistory: [
      { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
      { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
    ],
    // hospital: new Coding(),
    chartNo: '0042131241',
    idNo: 'L223456789',
    visitDate: new Date('2022-10-11'),
    visitSeqNo: '',
    visitSource: new Coding(),
    isFirstVisit: false,
    visitType: new Coding(),
    doctor: new Coding(),
    division: new Coding(),
    // roomNo: '000診',
    branch: new Coding(),
    room: '',
    seqNo: '100號',
    checkInTime: new Date(),
    plannedStartTime: new Date(),
    plannedEndTime: new Date('2999-12-31 23:59'),
    dischargeDisposition: new Coding(),
    currentStatus: new Coding(),
    caseType: new Coding(),
    isSensitive: false,
    chargeStatus: new Coding(),
    medCharge_id: '',
    remark: '',
    updatedBy: new Coding(),
    updatedAt: new Date(),
    visitTimeSlot: { code: '3', display: '晚上' },
    triage: new Coding,
  },
  {
    _id: '8bac8a8f-8888-88d8-88af-ea88f8bb888b',
    // nhiCardNo: '1234',
    nhiType: { code: '2', display: '一般健保' },
    partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    payType: { code: '1', display: '職業傷害' },
    therapyPlan: { code: '0', display: '非療程' },
    genType: { code: '1', display: '學院學生' },
    severity: { code: '不適用', display: '不適用' },
    severityHistory: [
      { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
      { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
    ],
    // hospital: new Coding(),
    chartNo: '0058183848',
    idNo: 'L228488888',
    visitDate: new Date('2021-12-08'),
    visitSeqNo: '',
    visitSource: new Coding(),
    isFirstVisit: false,
    visitType: new Coding(),
    doctor: new Coding(),
    division: new Coding(),
    // roomNo: '008診',
    branch: new Coding(),
    room: '',
    seqNo: '88號',
    checkInTime: new Date(),
    plannedStartTime: new Date(),
    plannedEndTime: new Date('2999-12-31 23:59'),
    dischargeDisposition: new Coding(),
    currentStatus: new Coding(),
    caseType: new Coding(),
    isSensitive: false,
    chargeStatus: new Coding(),
    medCharge_id: '',
    remark: '',
    updatedBy: new Coding(),
    updatedAt: new Date(),
    visitTimeSlot: { code: '2', display: '下午' },
    triage: new Coding,
  },
  {
    _id: '3bhc8h5f-1335-44d0-19hf-eh26f3bb636b',
    // nhiCardNo: '1234',
    nhiType: { code: '2', display: '一般健保' },
    partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
    payType: { code: '1', display: '職業傷害' },
    therapyPlan: { code: '0', display: '非療程' },
    genType: { code: '1', display: '學院學生' },
    severity: { code: '不適用', display: '不適用' },
    severityHistory: [
      { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
      { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
    ],
    // hospital: new Coding(),
    chartNo: '0012183856',
    idNo: 'M128486688',
    visitDate: new Date('2023-04-12'),
    visitSeqNo: '',
    visitSource: new Coding(),
    isFirstVisit: false,
    visitType: new Coding(),
    doctor: new Coding(),
    division: new Coding(),
    // roomNo: '306診',
    branch: new Coding(),
    room: '',
    seqNo: '8號',
    checkInTime: new Date(),
    plannedStartTime: new Date(),
    plannedEndTime: new Date('2999-12-31 23:59'),
    dischargeDisposition: new Coding(),
    currentStatus: new Coding(),
    caseType: new Coding(),
    isSensitive: false,
    chargeStatus: new Coding(),
    medCharge_id: '',
    remark: '',
    updatedBy: new Coding(),
    updatedAt: new Date(),
    visitTimeSlot: { code: '3', display: '晚上' },
    triage: new Coding,
  }
];

// TODO: Outpatient list 的資料異動，等確定再一次修正
export const mockPatients: Registration[] = [
  new Registration({
    _id: '4283f9fb-7b75-455a-8400-84a4f44cfa8b',
    visitDate: new Date('2023-04-11'),
    visitTimeSlot: { code: '2', display: '下午' },
    // roomNo: '305診',
    seqNo: '4號',
    name: '林明樺',
    chartNo: '0038842103',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1943/08/30'),
    // description: '#189162254',
    idNo: 'B123456789',
    // country: { code: 'TW', display: '台灣' },
    // address: [
    //   {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    //   {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    // ],
    // contact: {
    //   name: '林青山',
    //   relationship: { code: '1', display: '父子' },
    //   mobile: 900000000,
    //   homePhone: 424613322,
    //   physicalAddress: {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    // },
  }),
  new Registration({
    _id: '6bac6a6f-6666-66d6-66af-ea66f6bb666b',
    visitDate: new Date('2016-06-06'),
    visitTimeSlot: { code: '1', display: '早上' },
    // roomNo: '006診',
    seqNo: '66號',
    name: '童婷婷',
    chartNo: '0046161646',
    gender: { code: 'w', display: '女' },
    birthDate: new Date('2005/06/16'),
    // description: '#186867676',
    idNo: 'L226466686',
    // country: { code: 'AD', display: '安道爾公國' },
    // address: [
    //   {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    //   {
    //     cityDistrict: { code: '2', display: '台北市信義區' },
    //     street: '110台北市信義區信義路五段7號89樓',
    //   },
    // ],
    // contact: {
    //   name: '邱蚓',
    //   relationship: { code: '4', display: '母女' },
    //   mobile: 900256001,
    //   homePhone: 4294616224,
    //   physicalAddress: {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    // },
  }),
  new Registration({
    _id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
    visitDate: new Date('2022-10-11'),
    visitTimeSlot: { code: '3', display: '晚上' },
    // roomNo: '000診',
    seqNo: '100號',
    name: '邱蚓',
    chartNo: '0042131241',
    gender: { code: 'z', display: '女' },
    birthDate: new Date('1987/03/07'),
    // description: '#187877878',
    idNo: 'L223456789',
    // country: { code: 'TW', display: '台灣' },
    // address: [
    //   {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    //   {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    // ],
    // contact: {
    //   name: '童俊男',
    //   relationship: { code: '5', display: '夫妻' },
    //   mobile: 900550123,
    //   homePhone: 4294616224,
    //   physicalAddress: {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    // },
  }),
  new Registration({
    _id: '8bac8a8f-8888-88d8-88af-ea88f8bb888b',
    visitDate: new Date('2021-12-08'),
    visitTimeSlot: { code: '2', display: '下午' },
    // roomNo: '008診',
    seqNo: '88號',
    name: '童俊男',
    chartNo: '0058183848',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1985/08/18'),
    // description: '#1885887878',
    idNo: 'L228488888',
    // country: { code: 'AE', display: '阿拉伯聯合大公國' },
    // address: [
    //   {
    //     cityDistrict: { code: '3', display: '台東縣綠島鄉' },
    //     street: '951台東縣綠島鄉渔港路27號',
    //   },
    //   {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    // ],
    // contact: {
    //   name: '邱蚓',
    //   relationship: { code: '5', display: '夫妻' },
    //   mobile: 900256001,
    //   homePhone: 4294616224,
    //   physicalAddress: {
    //     cityDistrict: { code: '4', display: '台中市北區' },
    //     street: '404台中市北區雙十路一段65號',
    //   },
    // },
  }),
  new Registration({
    _id: '3bhc8h5f-1335-44d0-19hf-eh26f3bb636b',
    visitDate: new Date('2023-04-12'),
    visitTimeSlot: { code: '3', display: '晚上' },
    // roomNo: '306診',
    seqNo: '8號',
    name: '林青山',
    chartNo: '0012183856',
    gender: { code: 'x', display: '男' },
    birthDate: new Date('1923/03/9'),
    // description: '#186877679',
    idNo: 'M128486688',
    // country: { code: 'TW', display: '台灣' },
    // address: [
    //   {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    //   {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    // ],
    // contact: {
    //   name: '邱鷗',
    //   relationship: { code: '5', display: '夫妻' },
    //   mobile: 953257101,
    //   homePhone: 424613322,
    //   physicalAddress: {
    //     cityDistrict: { code: '1', display: '台中市西屯區' },
    //     street: '407台中市西屯區台灣大道二段166巷66巷8號',
    //   },
    // },
  }),
];

export const mockOpdMedOrders: OpdMedOrder = {
  soapNote: {
    _id: crypto.randomUUID(),
    medRecord_id: '',
    subjective: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum',
    objective: 'Vestibulum tempus imperdiet sem ac porttitor. Vivamus pulvinar commodo orci, suscipit porttitor velit elementum non. Fusce nec pellentesque erat, id lobortis nunc. Donec dui leo, ultrices quis turpis nec, sollicitudin sodales tortor. Aenean dapibus ',
    plan: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',
    assessment: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',
    updatedBy: new Coding(),
    updatedAt: new Date(),
  },

  diagOrders: [
    {
      _id: '123',
      medRecord_id: '',
      diag: {
        code: 'R53.83',
        display: 'Other fatigue',
      },
      type: new Coding(),
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
    {
      _id: '',
      medRecord_id: '',
      type: new Coding(),
      diag: {
        code: 'E11.65',
        display: 'DM poor control',
      },
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
    {
      _id: '',
      medRecord_id: '',
      type: new Coding(),
      diag: {
        code: 'E53.81',
        display: 'DM poor control',
      },
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
  ],
  examOrders: [
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '早晚飯後及睡前',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '05',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4a99c070-0e7f-4b98-91d0-77a9a20dd73f', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-00010', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: new Date()
    },
  ],
  drugOrders: [
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '123',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001',
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '抗1', display: '抗1' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '05',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '05',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-00010',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: new Date(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
  ],
};


export const mockOpdMedOrder: OpdMedOrder = {

  soapNote: new SoapNote({
    subjective: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum',
    objective: 'Vestibulum tempus imperdiet sem ac porttitor. Vivamus pulvinar commodo orci, suscipit porttitor velit elementum non. Fusce nec pellentesque erat, id lobortis nunc. Donec dui leo, ultrices quis turpis nec, sollicitudin sodales tortor. Aenean dapibus ',
    plan: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',
    assessment: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',

  }),
  diagOrders: [
    {
      _id: '123',
      medRecord_id: '',
      type: new Coding(),
      diag: {
        code: 'R53.83',
        display: 'Other fatigue',
      },
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
    {
      _id: '',
      medRecord_id: '',
      type: new Coding(),
      diag: {
        code: 'E11.65',
        display: 'DM poor control',
      },
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
    {
      _id: '',
      medRecord_id: '',
      type: new Coding(),
      diag: {
        code: 'E53.81',
        display: 'DM poor control',
      },
      isChronic: false,
      isRuleOut: false,
      orderUser: new Coding(),
      orderTime: new Date(),
      startUser: new Coding(),
      startTime: new Date(),
      endUser: new Coding(),
      endTime: Date.defaultTime(),
      updatedBy: new Coding(),
      updatedAt: new Date(),
    },
  ],
  // nhiRecord: {
  //   _id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
  //   nhiCardNo: '1234',
  //   nhiType: { code: '2', display: '一般健保' },
  //   partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
  //   payType: { code: '1', display: '職業傷害' },
  //   therapyPlan: { code: '0', display: '非療程' },
  //   genType: { code: '1', display: '學院學生' },
  //   severity: new Coding(),
  //   severityHistory: [
  //     { code: 'M32.0', display: '永久：M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)' },
  //     { code: 'F30.10', display: '永久：續發 F30.10 情感性疾患 Affective disorders' },
  //   ],
  //   hospital: new Coding(),
  //   chartNo: '',
  //   idNo: '',
  //   visitDate: new Date(),
  //   visitSeqNo: '',
  //   visitSource: new Coding(),
  //   isFirstVisit: false,
  //   visitType: new Coding(),
  //   doctor: new Coding(),
  //   division: new Coding(),
  //   roomNo: '',
  //   seqNo: '',
  //   checkInTime: new Date(),
  //   plannedStartTime: new Date(),
  //   plannedEndTime: new Date('2999-12-31 23:59'),
  //   dischargeDisposition: new Coding(),
  //   currentStatus: new Coding(),
  //   caseType: new Coding(),
  //   isSensitive: false,
  //   chargeStatus: new Coding(),
  //   medCharge_id: '',
  //   remark: '',
  //   updatedBy: new Coding(),
  //   updatedAt: new Date(),
  //   visitTimeSlot: new Coding(),
  //   triage: new Coding,
  // },
  drugOrders: [
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000100',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000101',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000102',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'drug', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '抗1', display: '抗1' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000103',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: 'route', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000104',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: '123', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '10',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'PO',
      }, // 途徑
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000105',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: '123', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '05',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'BIW',
        display: '每週兩次',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000106',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: '123', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
    {
      status: {
        code: '05',
        display: '開立等待覆核',
      }, // 醫囑狀態
      alertType: { code: '', display: '' }, // 警告類型
      invCode: '001', // 藥品名稱
      dosage: {
        value: 1,
        unit: 'Tab',
        code: '01',
      }, // 劑量與單位
      dose: {
        value: 300,
        unit: 'mg',
        code: '01',
      }, // 使用劑量
      route: {
        code: '01',
        display: 'IDV',
      }, // 途徑
      frequency: {
        code: 'BIW',
        display: '每週兩次',
      }, // 頻率
      isMill: true, // 磨粉
      advise: '', // 囑咐
      usedDays: 1, // 使用天數
      total: {
        value: 1,
        unit: 'Tab',
        code: '01',
      },
      isSelf: false, // 自費
      nhiPrice: 120, // 健保價、
      selfPrice: 500, // 自費價
      drug: {
        code: '82-00010',
        display: '82-00010', // 醫囑編號
      },
      // --------------------------------------------------------
      _id: '82-000107',
      orderSource: '123', // 來源醫囑編號
      seqNo: 0,
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
      medType: { code: '123', display: '123' },
      ingredQty: 0,
      ingredRatio: 100,
      capacityRatio: 0,
      volumeRatio: 0,
      preExecLoc: {
        code: '330',
        display: '林崇明',
      },
      chargeType: {
        code: '330',
        display: '林崇明',
      },
      costAdd: { code: '', display: '' },
      expandTime: new Date(),
      startTime: new Date(),
      startUser: {
        code: '330',
        display: '林崇明',
      },
      endTime: Date.defaultTime(),
      endUser: {
        code: '330',
        display: '林崇明',
      },
      orderTime: new Date(),
      orderUser: {
        code: '330',
        display: '林崇明',
      },
      updatedAt: new Date(),
      updatedBy: {
        code: '330',
        display: '林崇明',
      },
      confirmTime: new Date(),
      confirmUser: {
        code: '330',
        display: '林崇明',
      },
      medSummary_id: '',
      nhiCode: '330',
      isFirstDose: false,
    },
  ],
  examOrders: [
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'BID',
        display: '一日兩次',
      }, // 頻率
      medSummary_id: '1', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000100', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '2', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000101', // 醫囑編號
      status: {
        code: '05',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '3', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000102', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '4', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000103', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '5', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000104', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),

      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '6', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000105', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '7', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000106', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
    {
      invCode: '123',
      usedDays: 1, // 使用天數
      frequency: {
        code: 'STAT',
        display: '立即使用',
      }, // 頻率
      medSummary_id: '8', // Summary_id
      advise: '', // 醫師指示
      isSelf: false,
      nhiPrice: 120,
      selfPrice: 500,
      exam: {
        code: '82-00010',
        display: '82-00010',
      }, // 醫囑編號

      // --------------------------------
      nhiCode: '123',
      _id: '82-000107', // 醫囑編號
      status: {
        code: '10',
        display: '開立',
      },
      medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
      chargeType: {
        code: '01',
        display: '開立',
      }, // 檢驗類別
      dose: {
        value: 100,
        unit: 'Tab',
        code: '01',
      },
      route: {
        code: '01',
        display: 'IDV',
      },
      costAdd: {
        code: '01',
        display: 'IDV',
      },
      orderUser: {
        code: '01',
        display: '林崇明',
      },
      orderTime: new Date(),
      updatedBy: {
        code: '01',
        display: '林崇明',
      },
      updatedAt: new Date(),
      confirmUser: {
        code: '01',
        display: '林崇明',
      },
      confirmTime: new Date(),
      preExecLoc: {
        code: '01',
        display: '林崇明',
      },
      startUser: {
        code: '01',
        display: '林崇明',
      },
      expandTime: new Date(),
      endUser: {
        code: '01',
        display: '林崇明',
      },
      orderSource: '',
      medType: {
        code: "order",
        display: '123'
      },
      total: {
        code: '01',
        value: 1,
        unit: 'days'
      },
      seqNo: 0,
      startTime: new Date(), endTime: Date.defaultTime()
    },
  ],
};

// TODO: 修改 OutpatientNote 拼字問題
export const mockOutpatientNote: SoapNote = {
  medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
  _id: crypto.randomUUID(),
  subjective: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum',
  objective: 'Vestibulum tempus imperdiet sem ac porttitor. Vivamus pulvinar commodo orci, suscipit porttitor velit elementum non. Fusce nec pellentesque erat, id lobortis nunc. Donec dui leo, ultrices quis turpis nec, sollicitudin sodales tortor. Aenean dapibus ',
  assessment: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',
  plan: 'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, volutpat et tortor. In vulputate lorem quis dui vestibulum, vitae imperdiet diam bibendum. Maecenas scelerisque orci a dolor vestibulum sagittis. Etiam quis finibus arcu, vel efficitur diam. Curabitur felis eros, ',
  updatedAt: new Date(),
  updatedBy: new Coding()
};

export const mockNhiRecord: NhiRecord = {
  medicalRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
  card: '1234',
  nhiType: { code: '2', display: '一般健保' },
  partType: { code: 'A00', display: '急診', system: '西醫-醫學中心' },
  payType: { code: '1', display: '職業傷害' },
  course: { code: '0', display: '非療程' },
  genType: { code: '1', display: '學院學生' },
  severityHistory: [
    { code: 'M32.0', display: 'M32.0 全身性紅斑狼瘡 Systemic lupus erythematosus(SLE)', version: '永久：' },
    { code: 'F30.10', display: '續發 F30.10 情感性疾患 Affective disorders', version: '永久：' },
  ]
};

export const mockDrugOrders: DrugOrder[] = [
  {
    status: {
      code: '10',
      display: '開立等待覆核',
    }, // 醫囑狀態
    alertType: { code: '', display: '' }, // 警告類型
    invCode: '001', // 藥品名稱
    dosage: {
      value: 1,
      unit: 'Tab',
      code: '01',
    }, // 劑量與單位
    dose: {
      value: 300,
      unit: 'mg',
      code: '01',
    }, // 使用劑量
    route: {
      code: '01',
      display: 'IDV',
    }, // 途徑
    frequency: {
      code: 'STAT',
      display: '立即使用',
    }, // 頻率
    isMill: true, // 磨粉
    advise: '', // 囑咐
    usedDays: 1, // 使用天數
    total: {
      value: 1,
      unit: 'Tab',
      code: '01',
    },
    isSelf: false, // 自費
    nhiPrice: 120, // 健保價、
    selfPrice: 500, // 自費價
    drug: {
      code: '82-00010',
      display: '82-00010', // 醫囑編號
    },
    // --------------------------------------------------------
    _id: '82-00010',
    orderSource: '123', // 來源醫囑編號
    seqNo: 0,
    medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
    medType: { code: '123', display: '123' },
    ingredQty: 0,
    ingredRatio: 100,
    capacityRatio: 0,
    volumeRatio: 0,
    preExecLoc: {
      code: '330',
      display: '林崇明',
    },
    chargeType: {
      code: '330',
      display: '林崇明',
    },
    costAdd: { code: '', display: '' },
    expandTime: new Date(),
    startTime: new Date(),
    startUser: {
      code: '330',
      display: '林崇明',
    },
    endTime: new Date(),
    endUser: {
      code: '330',
      display: '林崇明',
    },
    orderTime: new Date(),
    orderUser: {
      code: '330',
      display: '林崇明',
    },
    updatedAt: new Date(),
    updatedBy: {
      code: '330',
      display: '林崇明',
    },
    confirmTime: new Date(),
    confirmUser: {
      code: '330',
      display: '林崇明',
    },
    medSummary_id: '',
    nhiCode: '330',
    isFirstDose: false,
  },
  {
    status: {
      code: '10',
      display: '開立等待覆核',
    }, // 醫囑狀態
    alertType: { code: '', display: '' }, // 警告類型
    invCode: '001', // 藥品名稱
    dosage: {
      value: 1,
      unit: 'Tab',
      code: '01',
    }, // 劑量與單位
    dose: {
      value: 300,
      unit: 'mg',
      code: '01',
    }, // 使用劑量
    route: {
      code: '01',
      display: 'IDV',
    }, // 途徑
    frequency: {
      code: 'STAT',
      display: '立即使用',
    }, // 頻率
    isMill: true, // 磨粉
    advise: '', // 囑咐
    usedDays: 1, // 使用天數
    total: {
      value: 1,
      unit: 'Tab',
      code: '01',
    },
    isSelf: false, // 自費
    nhiPrice: 120, // 健保價、
    selfPrice: 500, // 自費價
    drug: {
      code: '82-00010',
      display: '82-00010', // 醫囑編號
    },
    // --------------------------------------------------------
    _id: '82-00010',
    orderSource: '123', // 來源醫囑編號
    seqNo: 0,
    medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b',
    medType: { code: '123', display: '123' },
    ingredQty: 0,
    ingredRatio: 100,
    capacityRatio: 0,
    volumeRatio: 0,
    preExecLoc: {
      code: '330',
      display: '林崇明',
    },
    chargeType: {
      code: '330',
      display: '林崇明',
    },
    costAdd: { code: '', display: '' },
    expandTime: new Date(),
    startTime: new Date(),
    startUser: {
      code: '330',
      display: '林崇明',
    },
    endTime: new Date(),
    endUser: {
      code: '330',
      display: '林崇明',
    },
    orderTime: new Date(),
    orderUser: {
      code: '330',
      display: '林崇明',
    },
    updatedAt: new Date(),
    updatedBy: {
      code: '330',
      display: '林崇明',
    },
    confirmTime: new Date(),
    confirmUser: {
      code: '330',
      display: '林崇明',
    },
    medSummary_id: '',
    nhiCode: '330',
    isFirstDose: false,
  },
];

export const mockExamOrders: ExamOrder[] = [
  {
    invCode: '123',
    usedDays: 1, // 使用天數
    frequency: {
      code: 'STAT',
      display: '立即使用',
    }, // 頻率
    medSummary_id: '', // Summary_id
    advise: '', // 醫師指示
    isSelf: false,
    nhiPrice: 120,
    selfPrice: 500,
    exam: {
      code: '82-00010',
      display: '82-00010',
    }, // 醫囑編號

    // --------------------------------
    nhiCode: '123',
    _id: '82-00010', // 醫囑編號
    status: {
      code: '10',
      display: '開立',
    },
    medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
    chargeType: {
      code: '01',
      display: '開立',
    }, // 檢驗類別
    dose: {
      value: 100,
      unit: 'Tab',
      code: '01',
    },
    route: {
      code: '01',
      display: 'IDV',
    },
    costAdd: {
      code: '01',
      display: 'IDV',
    },
    orderUser: {
      code: '01',
      display: '林崇明',
    },
    orderTime: new Date(),
    updatedBy: {
      code: '01',
      display: '林崇明',
    },
    updatedAt: new Date(),
    confirmUser: {
      code: '01',
      display: '林崇明',
    },
    confirmTime: new Date(),
    preExecLoc: {
      code: '01',
      display: '林崇明',
    },
    startUser: {
      code: '01',
      display: '林崇明',
    },
    expandTime: new Date(),

    endUser: {
      code: '01',
      display: '林崇明',
    },
    orderSource: '',
    medType: {
      code: "order",
      display: '123'
    },
    total: {
      code: '01',
      value: 1,
      unit: 'days'
    },
    seqNo: 0,
    startTime: new Date(), endTime: new Date()
  },
  {
    invCode: '123',
    usedDays: 1, // 使用天數
    frequency: {
      code: 'STAT',
      display: '立即使用',
    }, // 頻率
    medSummary_id: '', // Summary_id
    advise: '', // 醫師指示
    isSelf: false,
    nhiPrice: 120,
    selfPrice: 500,
    exam: {
      code: '82-00010',
      display: '82-00010',
    }, // 醫囑編號

    // --------------------------------
    nhiCode: '123',
    _id: '82-00010', // 醫囑編號
    status: {
      code: '10',
      display: '開立',
    },
    medRecord_id: '4bac8a5f-9345-44d0-99af-ea26f3bb636b', // 就醫編號
    chargeType: {
      code: '01',
      display: '開立',
    }, // 檢驗類別
    dose: {
      value: 100,
      unit: 'Tab',
      code: '01',
    },
    route: {
      code: '01',
      display: 'IDV',
    },
    costAdd: {
      code: '01',
      display: 'IDV',
    },
    orderUser: {
      code: '01',
      display: '林崇明',
    },
    orderTime: new Date(),
    updatedBy: {
      code: '01',
      display: '林崇明',
    },
    updatedAt: new Date(),
    confirmUser: {
      code: '01',
      display: '林崇明',
    },
    confirmTime: new Date(),
    preExecLoc: {
      code: '01',
      display: '林崇明',
    },
    startUser: {
      code: '01',
      display: '林崇明',
    },
    expandTime: new Date(),
    endUser: {
      code: '01',
      display: '林崇明',
    },
    orderSource: '',
    medType: {
      code: "order",
      display: '123'
    },
    total: {
      code: '01',
      value: 1,
      unit: 'days'
    },
    seqNo: 0,
    startTime: new Date(), endTime: new Date()
  },
];

export const mockDiagRecords: DiagOrder[] = [
  {
    _id: '',
    medRecord_id: '',
    type: new Coding(),
    diag: {
      code: 'R53.83',
      display: 'Other fatigue',
    },
    isChronic: false,
    isRuleOut: false,
    orderUser: new Coding(),
    orderTime: new Date(),
    startUser: new Coding(),
    startTime: new Date(),
    endUser: new Coding(),
    endTime: new Date(),
    updatedBy: new Coding(),
    updatedAt: new Date(),
  },
  {
    _id: '',
    medRecord_id: '',
    type: new Coding(),
    diag: {
      code: 'E11.65',
      display: 'DM poor control',
    },
    isChronic: false,
    isRuleOut: false,
    orderUser: new Coding(),
    orderTime: new Date(),
    startUser: new Coding(),
    startTime: new Date(),
    endUser: new Coding(),
    endTime: new Date(),
    updatedBy: new Coding(),
    updatedAt: new Date(),
  },
  {
    _id: '',
    medRecord_id: '',
    type: new Coding(),
    diag: {
      code: 'E53.81',
      display: 'DM poor control',
    },
    isChronic: false,
    isRuleOut: false,
    orderUser: new Coding(),
    orderTime: new Date(),
    startUser: new Coding(),
    startTime: new Date(),
    endUser: new Coding(),
    endTime: new Date(),
    updatedBy: new Coding(),
    updatedAt: new Date(),
  },
];

export const mockGroupValue2 = [

  {

    "info": {

      "code": "Injection",

      "display": "注射"

    }

  },

  {

    "info": {

      "code": "ExternalUse",

      "display": "外用"

    }

  },

  {

    "info": {

      "code": "Eye",

      "display": "眼用"

    }

  },

  {

    "info": {

      "code": "Nutrition",

      "display": "營養品"

    }

  },

  {

    "info": {

      "code": "Cosmetics",

      "display": "美妝"

    }

  }

];
export const mockGroupValue: CartGroup[] = [

  {

    "info": {

      "code": "5000101",

      "display": "急作檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5010101",

          "display": "凝固因子"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010102",

          "display": "\"尿液.體液鏡檢 \""

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010103",

          "display": "血液"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010104",

          "display": "生化(血液)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010105",

          "display": "生化(尿液)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010106",

          "display": "生化(體液)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010107",

          "display": "血液氣體分析"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010108",

          "display": "藥物濃度監控"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010109",

          "display": "血庫"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010110",

          "display": "快篩檢驗"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000102",

      "display": "血庫檢驗"

    }

  },

  {

    "info": {

      "code": "5000103",

      "display": "分子檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5010301",

          "display": "病原性分子檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010302",

          "display": "遺傳性分子檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010303",

          "display": "\"HLA分子檢驗 \""

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010304",

          "display": "癌症基因分子檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010305",

          "display": "疾病風險基因分子檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010306",

          "display": "委外項目"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000104",

      "display": "微生物"

    },

    "subGroups": [

      {

        "info": {

          "code": "5010401",

          "display": "細菌培養(嗜氧)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010402",

          "display": "細菌培養(嗜氧及厭氧)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010403",

          "display": "結核菌培養"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010404",

          "display": "黴菌培養"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010405",

          "display": "直接鏡檢染色"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010406",

          "display": "細菌抗原試驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010407",

          "display": "病毒培養"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000106",

      "display": "生化檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5010601",

          "display": "生化(血液)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010602",

          "display": "生化(尿液)"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010603",

          "display": "\"生化(CSF) \""

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5010604",

          "display": "生化(體液)"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000107",

      "display": "特殊生化檢驗"

    }

  },

  {

    "info": {

      "code": "5000108",

      "display": "血清免疫"

    }

  },

  {

    "info": {

      "code": "5000109",

      "display": "藥物濃度檢驗"

    }

  },

  {

    "info": {

      "code": "5000110",

      "display": "血液檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5110021",

          "display": "血液常規"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5110022",

          "display": "血凝檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5110023",

          "display": "特殊血液"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5110024",

          "display": "骨髓檢驗"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000111",

      "display": "尿液糞便體液鏡檢"

    },

    "subGroups": [

      {

        "info": {

          "code": "5111011",

          "display": "尿液"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5111013",

          "display": "糞便檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5111014",

          "display": "體液"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000112",

      "display": "精準醫學檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5011201",

          "display": "罕病及難以診斷疾病基因檢測"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011202",

          "display": "微生物及免疫體檢測"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011203",

          "display": "癌症基因體及不同癌基因組織檢測"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011204",

          "display": "各種癌特定基因組檢測"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011205",

          "display": "標靶治療相關的基因組"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011206",

          "display": "液態切片"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011207",

          "display": "癌相關單基因檢測"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000113",

      "display": "毒物檢驗"

    },

    "subGroups": [

      {

        "info": {

          "code": "5011301",

          "display": "農藥物檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011302",

          "display": "有機溶劑檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011303",

          "display": "血液重金屬檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5011304",

          "display": "尿液重金屬檢驗"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000003",

      "display": "常用"

    },

    "subGroups": [

      {

        "info": {

          "code": "5000301",

          "display": "血液"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000302",

          "display": "鏡檢"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000303",

          "display": "生化"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000304",

          "display": "血清免疫"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000305",

          "display": "細菌"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000306",

          "display": "病毒"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000307",

          "display": "分子檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000308",

          "display": "血庫檢驗"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000309",

          "display": "委外"

        },


        "isChecked": false

      },

      {

        "info": {

          "code": "5000310",

          "display": "其他"

        },


        "isChecked": false

      }

    ]

  },

  {

    "info": {

      "code": "5000201",

      "display": "床邊檢驗"

    }

  }

];

export const mockItemValues2 = [

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010101',

      display: '凝固因子',

    },

    infos: [

      {

        code: 'W0000103',

        display: 'Fibrinogen',

      },

      {

        code: 'W0000107',

        display: 'PT',

      },

      {

        code: 'W0000114',

        display: 'APTT',

      },

      {

        code: 'W0000136',

        display: 'D-Dimer',

      },

      {

        code: 'W0000141',

        display: 'Mixing PT test',

      },

      {

        code: 'W0000144',

        display: 'Mixing APTT test',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010102',

      display: '尿液.體液鏡檢',

    },

    infos: [

      {

        code: 'W0000029',

        display: 'Urine Sediment',

      },

      {

        code: 'W0000034',

        display: 'Urine routine',

      },

      {

        code: 'W0000036',

        display: 'Urine Chemistry',

      },

      {

        code: 'W0000043',

        display: 'Pregnancy Test',

      },

      {

        code: 'W0000772',

        display: 'Ascites fluid Routine',

      },

      {

        code: 'W0000775',

        display: 'Pleural fluid Routine',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010103',

      display: '血液',

    },

    infos: [

      {

        code: 'W0000066',

        display: 'RBC',

      },

      {

        code: 'W0000068',

        display: 'WBC',

      },

      {

        code: 'W0000070',

        display: 'Hb',

      },

      {

        code: 'W0000082',

        display: 'ESR',

      },

      {

        code: 'W0000084',

        display: 'Platelet',

      },

      {

        code: 'W0000087',

        display: 'Reticulocyte',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010104',

      display: '生化(血液)',

    },

    infos: [

      {

        code: 'W0000133',

        display: 'Osmolality',

      },

      {

        code: 'W0000164',

        display: 'BUN',

      },

      {

        code: 'W0000187',

        display: 'Glucose PC',

      },

      {

        code: 'W0000188',

        display: 'Glucose (random)',

      },

      {

        code: 'W0000189',

        display: 'Glucose AC',

      },

      {

        code: 'W0000200',

        display: 'Calcium(Ca)',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010105',

      display: '生化(尿液)',

    },

    infos: [

      {

        code: 'W0000018',

        display: 'Glucose',

      },

      {

        code: 'W0000041',

        display: 'Osmolality',

      },

      {

        code: 'W0000165',

        display: 'BUN(Urine)',

      },

      {

        code: 'W0000201',

        display: 'Calcium(Ca)',

      },

      {

        code: 'W0000222',

        display: 'Tablase',

      },

      {

        code: 'W0000236',

        display: 'Potassium(K)',

      },

      {

        code: 'W0000242',

        display: 'Chloride(Cl)',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010106',

      display: '生化(體液)',

    },

    infos: [

      {

        code: 'W0000190',

        display: 'Glucose(CSF)',

      },

      {

        code: 'W0000278',

        display: 'Micro-Total Protein(M-TP)',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010107',

      display: '血液氣體分析',

    },

    infos: [

      {

        code: 'W0000116',

        display: 'Met-Hb',

      },

      {

        code: 'W0000292',

        display: 'Blood Gas-Artery【★檢驗部操作★】',

      },

      {

        code: 'W0000293',

        display: 'Blood Gas-vein【★檢驗科操作★】',

      },

      {

        code: 'W0000402',

        display: 'COHb',

      },

      {

        code: 'W0001046',

        display: 'Plasma Free Calcium',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010108',

      display: '藥物濃度監控',

    },

    infos: [

      {

        code: 'W0000386',

        display: 'Carbamazepine(Tegretol)',

      },

      {

        code: 'W0000387',

        display: 'Dilantin(Phenytoin)',

      },

      {

        code: 'W0000388',

        display: 'MTX(Methotrexate)',

      },

      {

        code: 'W0000389',

        display: 'Salicylate',

      },

      {

        code: 'W0000390',

        display: 'Aminophylline',

      },

      {

        code: 'W0000391',

        display: 'Depakin(Valproic acid)',

      },

      {

        code: 'W0000392',

        display: 'Digoxin',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010109',

      display: '血庫',

    },

    infos: [

      {

        code: 'W0000424',

        display: 'ABO Typing',

      },

      {

        code: 'W0000425',

        display: 'ABO+Rh Typing',

      },

      {

        code: 'W0000427',

        display: 'Rh Grouping',

      }

    ],

  },

  {

    group: {

      code: '5000101',

      display: '急作檢驗',

    },

    subGroup: {

      code: '5010110',

      display: '快篩檢驗',

    },

    infos: [

      {

        code: 'W0000452',

        display: 'Mycoplasma pneumonia IgM (Rapid Test)',

      },

      {

        code: 'W0000538',

        display: 'Streptococcus Group A antigen rapid test',

      },

      {

        code: 'W0000724',

        display: 'EV71- IgM (Rapid-Test)',

      },

      {

        code: 'W0000725',

        display: 'RSV-Ag',

      },

      {

        code: 'IgM',

        display: 'Dengue fever NS1',

      },

      {

        code: 'W0000728',

        display: 'Adenovirus antigen rapid test',

      },

      {

        code: 'W0000729',

        display: '流感病毒抗原快速篩檢(健保)',

      },

      {

        code: 'W0000851',

        display: '流感病毒抗原快速篩檢(自費)',

      }

    ],

  },

  {

    group: {

      code: '5000102',

      display: '血庫檢驗',

    },

    infos: [

      {

        code: 'W0000420',

        display: 'ABO+Rh Typing',

      },

      {

        code: 'W0000423',

        display: 'ABO Typing',

      },

      {

        code: 'W0000426',

        display: 'Rh Typing',

      },

      {

        code: 'W0000428',

        display: 'Antibody Screening test',

      },

      {

        code: 'W0000429',

        display: 'Antibody Identification Test',

      },

      {

        code: 'W0000430',

        display: 'Elution and antibody identification',

      },

      {

        code: 'W0000432',

        display: 'Platelet antibody test(Indirect)',

      },

      {

        code: 'W0000433',

        display: 'Lewis antigen',

      },

      {

        code: 'W0000434',

        display: 'D.E.C.e.c',

      },

      {

        code: 'W0000436',

        display: 'Special blood group studies',

      },

      {

        code: 'W0000437',

        display: 'Saliva test',

      },

      {

        code: 'W0000438',

        display: 'Investigation transfusion reaction',

      },

      {

        code: 'W0000506',

        display: 'Direct Coomb’s polyspecific test',

      },

      {

        code: 'W0000507',

        display: 'Indirect Coomb’s polyspecific test',

      },

      {

        code: 'W0001229',

        display: '空血袋',

      },

      {

        code: 'W0001230',

        display: '小兒分袋血little blood improve(空袋費用+小血袋無菌分裝處理費)',

      },

      {

        code: 'W0002805',

        display: '照光血品(批價用)',

      }

    ],

  },

  {

    group: {

      code: '5000103',

      display: '分子檢驗',

    },

    subGroup: {

      code: '5010301',

      display: '病原性分子檢驗',

    },

    infos: [

      {

        code: 'W0000551',

        display: 'CMV PCR',

      },

      {

        code: 'W0000553',

        display: '砂眼披衣菌/淋病雙球菌核酸C. trachomatis / N. gonorrhoeae test',

      },

      {

        code: 'W0000556',

        display: 'HSV PCR',

      },

      {

        code: 'W0000557',

        display: 'EBV PCR',

      },

      {

        code: 'W0000558',

        display: 'Parvovirus B19 PCR',

      },

      {

        code: 'W0000559',

        display: 'PJP  PCR',

      }

    ],

  },

  {

    group: {

      code: '5000103',

      display: '分子檢驗',

    },

    subGroup: {

      code: '5010302',

      display: '遺傳性分子檢驗',

    },

    infos: [

      {

        code: 'W0000796',

        display: 'Apert^s syndrome(自費2500)',

      },

      {

        code: 'W0000798',

        display: 'Hypochondroplasia(自費2500)',

      },

      {

        code: 'W0000875',

        display: 'PARK2(Parkin)基因檢測(自費4000)',

      },

      {

        code: 'W0000876',

        display: 'PARK6(PINK1)基因檢測(自費4000)',

      },

      {

        code: 'W0000973',

        display: 'KCNQ1 gene(LQT1;Long QT syndrome 1)(自費13000)',

      },

      {

        code: '自費2200',

        display: 'SMA-Blood(委外)',

      },

      {

        code: 'W0000989',

        display: 'CSF3R (T618I)gene mutation analysis(自費2100元)',

      },

      {

        code: 'W0001016',

        display: 'Pigmented paravenous chorioretinal atrophy gene(自費3000)',

      },

      {

        code: 'W0001023',

        display: 'MELAS症候群突變基因(m.3243AG mitochondrial DNA mutation)自費3500',

      },

      {

        code: 'W0001024',

        display: 'MERRF症候群突變基因(m.8344AG mitochondrial DNA mutation)自費3500',

      },

      {

        code: 'W0001084',

        display: 'Achondroplasia PCR FGFR3 G380R限血液檢體(自費1500)',

      },

      {

        code: 'W0001086',

        display: 'Thalassemia PCR(羊水補助)',

      },

      {

        code: 'W0001950',

        display: 'Harmony (雙胞胎適用)(委外自費15000元)',

      },

      {

        code: 'W0001953',

        display: '(SMA+FXS) SMA+Fragile X syndrome (委外自費5000元)',

      }

    ],

  },

  {

    group: {

      code: '5000103',

      display: '分子檢驗',

    },

    subGroup: {

      code: '5010303',

      display: 'HLA分子檢驗',

    },

    infos: [

      {

        code: 'W0000500',

        display: 'HLA-B27',

      },

      {

        code: 'W0000501',

        display: 'HLA-ABC',

      },

      {

        code: 'W0000504',

        display: 'HLA-DR',

      },

      {

        code: 'W0000577',

        display: 'HLA-B1502 gene typing',

      },

      {

        code: 'W0000899',

        display: 'HLA-B*5801 gene typing(自費4200)',

      }

    ],

  },

  {

    group: {

      code: '5010304',

      display: '癌症基因分子檢驗',

    },

    subGroup: {

      code: '5010303',

      display: 'HLA分子檢驗',

    },

    infos: [

      {

        code: 'W0000552',

        display: 'TPMT*3C gene mutation analysis',

      },

      {

        code: 'W0000560',

        display: 'FLT3/ITD gene mutation assay',

      },

      {

        code: 'W0000561',

        display: 'BRAF V600E gene mutation analysis',

      },

      {

        code: 'W0000562',

        display: 'PIK3CA gene exon9 mutation analysis',

      },

      {

        code: 'W0000571',

        display: 'BCR-ABL定性及定量檢驗(p190+p210+p230)',

      },

      {

        code: 'W0000583',

        display: 'BCR-ABL基因定量檢驗',

      },

      {

        code: 'W0000872',

        display: 'EGFR(Cell Free DNA) exon18-21 mutation analysis(自費10000)',

      },

      {

        code: 'Codon13) mutation analysis(自費3000)',

        display: 'KRAS gene(Blood)(Codon12)',

      },

      {

        code: 'W0000878',

        display: 'NPM1突變基因檢驗(自費2000)',

      }

    ],

  },

  {

    group: {

      code: '5000103',

      display: '分子檢驗',

    },

    subGroup: {

      code: '5010305',

      display: '疾病風險基因分子檢驗',

    },

    infos: [

      {

        code: 'W0000829',

        display: '營養代謝基因任選一項(自費1800)',

      },

      {

        code: 'W0000830',

        display: '營養代謝基因任選二項(自費3600)',

      },

      {

        code: 'W0000831',

        display: '營養代謝基因任選三項(自費4500)',

      },

      {

        code: 'W0000832',

        display: '營養代謝基因任選四項(自費5700)',

      },

      {

        code: 'W0000833',

        display: '營養代謝基因任選五項(自費6900)',

      },

      {

        code: 'W0000834',

        display: '營養代謝基因任選六項(自費8100)',

      },

      {

        code: 'W0000976',

        display: 'eNOS-G894T gene mutation analysis(自費2500)',

      },

      {

        code: 'ACE-I/D gene mutation analysis(自費2500)',

        display: 'W0000977',

      },

      {

        code: 'W0000978',

        display: 'ApoE genotype(自費3500)',

      },

      {

        code: 'W0000979',

        display: 'GCLM-558 gene(自費3500)',

      },

      {

        code: 'W0000980',

        display: 'LOX1-0283 gene(自費3500)',

      },

      {

        code: 'ACE',

        display: 'Panel(五項):eNOS',

      },

      {

        code: 'W0001011',

        display: '腸道宏觀菌相篩檢 Microbiome(自費10000元)',

      }

    ],

  }

];


export const mockItemValues: CartMenuItem[] = [
  {
    group: {
      code: 'Injection',
      display: '注射',
    },
    infos: [
      {
        code: 'I10DX   ',
        display: 'Glucose 10% 500mL/Bot',
      },
      {
        code: 'I10DX   ',
        display: 'Glucose 10% 500mL/Bot',
      },
      {
        code: 'I10KCL-S',
        display: 'KCL 10mEq(0.149%) in Saline 500mL/Bot',
      },
      {
        code: 'I15BA20 ',
        display: 'Balance 1.5%Glucose 2L/Bot',
      },
      {
        code: 'I15BA25 ',
        display: 'Balance 1.5%Glucose 2.5L/Bot',
      },
      {
        code: 'I15BA50 ',
        display: 'Balance 1.5%Glucose 5L/Bot',
      },
      {
        code: 'I15D20LU',
        display: 'Dianeal D1.5% 2.0L/Bot(Low Ca.)UltraBag',
      },
      {
        code: 'I15D20U ',
        display: 'Dextrose ultrabag 1.5% 2000mL/Bot',
      },
      {
        code: 'I15D25  ',
        display: 'Dextrose 1.5% 2.5L/Bot機器',
      },
      {
        code: 'I15D25LU',
        display: 'Dianeal D1.5% 2.5L/Bot(low Ca.)Ultrabag',
      },
    ],
  },
  {
    group: {
      code: 'ExternalUse',
      display: '外用',
    },
    infos: [
      {
        code: 'ICUROSU ',
        display: 'phospholipid fraction 240mg/Vial(醫免)',
      },
      {
        code: 'LAMORO2 ',
        display: 'Amorolfine nail lacquer 5% 2.5mL/Bot',
      },
      {
        code: 'LATRO-S ',
        display: 'Ipratropium(吸入-綠)500mcg/2mL/Amp',
      },
      {
        code: 'LAZELA1 ',
        display: 'BENZYDAMINE 3mg/mL 15mL/Bot',
      },
      {
        code: 'LBENZY2 ',
        display: 'Balance 1.5%Glucose 2.5L/Bot',
      },
      {
        code: 'LBENZY21 ',
        display: 'BENZYDAMINE 3mg/mL 15mL/Bot',
      },
      {
        code: 'LBERODU ',
        display: 'beroDUAL N綠(複方) 200dose/10mL/Bot',
      },
      {
        code: 'LBETAME ',
        display: 'Betamethasone(膠液)Soln 0.064% 10g/Bot',
      },
      {
        code: 'LBEVESP ',
        display: 'Bevespi Aerosphere 7.2/5mcg 120puffs/Bot',
      },
      {
        code: 'LBIW201 ',
        display: '出院-金碘藥水 200mL/Bot',
      },
    ],
  },
  {
    group: {
      code: 'Eye',
      display: '眼用',
    },
    infos: [
      {
        code: 'L2ISOPT ',
        display: 'Pilocarpine eye solution 2% 15mL/Bot',
      },
      {
        code: 'LARTELA ',
        display: 'Artelac Eye Drops 10mL/Bot(複方)',
      },
      {
        code: 'LATRO11 ',
        display: 'Atropine 0.125% eye drops 5mL/Bot',
      },
      {
        code: 'LATRO13 ',
        display: 'Atropine 0.01% eye drops 30Amp/Box',
      },
      {
        code: 'LATRO131',
        display: 'Atropine 0.01% eye drops 5mL/Bot',
      },
      {
        code: 'LATRO32 ',
        display: 'Atropine 0.3% EYE drops  5mL/Bot',
      },
      {
        code: 'LATRO52 ',
        display: 'Atropine 0.5% eye drops 10mL/Bot',
      },
      {
        code: 'LATROP11',
        display: 'Atropine 1% oph. 10mL/Bot',
      },
      {
        code: 'LAZARGA ',
        display: 'Azarga oph susp.(複方)5mL/Bot',
      },
      {
        code: 'LBIMAT3 ',
        display: 'Bimatoprost 0.03% eye drops 0.4mL/Amp',
      },
    ],
  },
  {
    group: {
      code: 'Nutrition',
      display: '營養品',
    },
    infos: [
      {
        code: 'FABI31 ',
        display: 'AB-i3.1  435.9mg/Cap',
      },
      {
        code: 'FAHA301 ',
        display: 'A+HA極品玻尿酸 30Bot/Box',
      },
      {
        code: 'FAMI141 ',
        display: '黃金級胺基酸粉末 14packs/Box',
      },
      {
        code: 'FAMI301 ',
        display: '專業級胺基酸粉末 30Packs/Box',
      },
      {
        code: 'FARE601 ',
        display: '歐銳適金盞花萃取葉黃素複方軟膠囊60Tabs',
      },
      {
        code: 'FBAB501 ',
        display: 'Baby vitamins 50mL/Bot',
      },
      {
        code: 'FBIO201 ',
        display: '鎂溶易400mg/Tube 20Tube/Box',
      },
      {
        code: 'FBIO301 ',
        display: 'Biogaia錠劑 30Tabs/Box',
      },
      {
        code: 'FBIO601 ',
        display: '金牌門積門山米蕈SV多醣體 60 Cap/Box',
      },
      {
        code: 'FBIOGA2 ',
        display: 'Biogaia protectis 10mL/Bot',
      },
    ],
  },
  {
    group: {
      code: 'Cosmetics',
      display: '美妝',
    },
    infos: [
      {
        code: 'CACCU10 ',
        display: '羅氏智航血糖健保試紙 10片/盒',
      },
      {
        code: 'CACCU12 ',
        display: '羅氏智航血糖健保試紙  10片/盒',
      },
      {
        code: 'CACCU50 ',
        display: '羅氏智航血糖健保試紙  50片/盒',
      },
      {
        code: 'CACCU51 ',
        display: '羅氏智航血糖試紙(妊娠用)  50片/盒',
      },
      {
        code: 'CACCUC1 ',
        display: '羅氏智航血糖試紙(妊娠用) 50片/盒',
      },
      {
        code: 'CACCUCH ',
        display: '羅氏智航血糖健保試紙 50片/盒',
      },
      {
        code: 'CAHA151 ',
        display: 'C-果酸露15 15mL/Box',
      },
      {
        code: 'CAKN201 ',
        display: 'C-祛痘潔面乳 200mL/Box',
      },
      {
        code: 'CANS041 ',
        display: '安適康疤痕護理矽膠筆4g/Box',
      },
      {
        code: 'CANS151 ',
        display: '安適康皙佳疤痕護理矽凝膠 15g/Box',
      },
    ],
  },
];

export const mockOrderInfos: ExamOrder[] = [
  new ExamOrder({
    medRecord_id: 'RBC',
    exam: { code: 'ITEICOP', display: 'ITEICOP' },
    dose: { value: 1, unit: '次', code: 'time' },
    advise: '急診:40分鐘;門診:40分鐘;住院:2小時',
    medType: { code: 'Pee', display: '尿液檢驗單' }
  }),
  new ExamOrder({
    medRecord_id: 'WBC',
    exam: { code: 'ITEICOP', display: 'ITEICOP' },
    dose: { value: 2, unit: '回', code: 'rund' },
    advise: '急診:40分鐘;門診:40分鐘;住院:2小時',
    medType: { code: 'Pee', display: '尿液檢驗單' }
  }),
  new ExamOrder({
    medRecord_id: 'CBC',
    exam: { code: 'BLDCPOE', display: 'BLDCPOE' },
    dose: { value: 3, unit: '瓶', code: 'bottle' },
    advise: '急診:40分鐘;門診:40分鐘;住院:2小時',
    medType: { code: 'Blood', display: '血液檢驗單' }
  }),
  new ExamOrder({
    medRecord_id: 'MCV',
    exam: { code: 'BLDCPOE', display: 'BLDCPOE' },
    dose: { value: 4, unit: '罐', code: 'can' },
    advise: '急診:40分鐘;門診:40分鐘;住院:2小時',
    medType: { code: 'Blood', display: '血液檢驗單' }
  }),
];

export const mockDiagnosisQuery: DiagItem[] = [
  {
    code: "A00.9",
    display: "霍亂，未明確",
    displayEnglish: "Cholera, unspecified"
  },
  {
    code: "A01.0",
    display: "傷寒",
    displayEnglish: "Typhoid fever"
  },
  {
    code: "A01.1",
    display: "甲型副傷寒",
    displayEnglish: "Paratyphoid fever A"
  },
  {
    code: "A01.3",
    display: "副傷寒丙型",
    displayEnglish: "Paratyphoid fever C"
  },
  {
    code: "A30.0",
    display: "不確定的麻風病",
    displayEnglish: "Indeterminate leprosy"
  },
  {
    code: "A30.1",
    display: "結核性痲瘋病",
    displayEnglish: "Tuberculoid leprosy"
  },
  {
    code: "A30.3",
    display: "邊緣性麻風病",
    displayEnglish: "Borderline leprosy"
  },
  {
    code: "A33",
    display: "新生兒破傷風",
    displayEnglish: "Tetanus neonatorum"
  },
  {
    code: "A34",
    display: "產科破傷風 ",
    displayEnglish: "Obstetrical tetanus"
  },
  {
    code: "B95.0",
    display: "A 組鏈球菌，作為分類到其他章節的疾病的病因",
    displayEnglish: "Streptococcus, group A, as the cause of diseases classified to other chapters"
  },
  {
    code: "A95.1",
    display: "B 組鏈球菌，作為分類到其他章節的疾病的病因",
    displayEnglish: "Streptococcus, group B, as the cause of diseases classified to other chapters"
  },
  {
    code: "B95.2",
    display: "D 型鏈球菌和腸球菌作為分類到其他章節的疾病的病因",
    displayEnglish: "Streptococcus group D and enterococcus as the cause of diseases classified to other chapters"
  },
  {
    code: "B98.0",
    display: "幽門螺旋桿菌 [H.pylori] 作為分類到其他章節的疾病的病因",
    displayEnglish: "Helicobacter pylori [H.pylori] as the cause of diseases classified to other chapters"
  },
  {
    code: "B98.1",
    display: "創傷弧菌作為分類到其他章節的疾病的病因",
    displayEnglish: "Vibrio vulnificus as the cause of diseases classified to other chapters"
  },
  {
    code: "Pipistrellus",
    display: "管鼻蝠屬",
    displayEnglish: "Pipistrelle Bat"
  },
  {
    code: "J11.0",
    display: "流感伴肺炎，病毒未鑑定",
    displayEnglish: "Influenza with pneumonia, virus not identified"
  },
  {
    code: "J11.1",
    display: "具有其他呼吸道症狀的流感，病毒尚未確定",
    displayEnglish: "Influenza with other respiratory manifestations, virus not identified"
  },
  {
    code: "J12.0",
    display: "腺病毒肺炎",
    displayEnglish: "Adenoviral pneumonia"
  },
  {
    code: "J12.1",
    display: "呼吸道合胞病毒肺炎",
    displayEnglish: "Respiratory syncytial virus pneumonia"
  },
  {
    code: "J12.2",
    display: "副流感病毒肺炎",
    displayEnglish: "Parainfluenza virus pneumonia"
  },
  {
    code: "J90",
    display: "胸腔積液，未分類",
    displayEnglish: "Pleural effusion, not elsewhere classified"
  },
  {
    code: "J91",
    display: "分類為其他疾病的胸腔積液",
    displayEnglish: "Pleural effusion in conditions classified elsewhere"
  }
];

export const mockDiagnosisAI: DiagItem[] = [
  {
    code: 'I10',
    display: "本癥性（原癥性）高血壓",
    displayEnglish: 'Eseential (primary) hypetension',
    predictedValue: 100
  },
  {
    code: 'N40.0',
    display: "膀胱下段症狀缺乏的前列腺肥大",
    displayEnglish: 'Enlarged prostate without lower urinar tract symptoms',
    predictedValue: 100
  },
  {
    code: 'F01.5',
    display: "無行為異常的血管性失智症",
    displayEnglish: 'Vascular dementia without behavioral disturbance',
    predictedValue: 99
  },
  {
    code: 'E11.9',
    display: "2型糖尿病無併發症",
    displayEnglish: 'Type 2 diabetes mellitus without complications',
    predictedValue: 99
  },
  {
    code: 'E32.9',
    display: "未明示胸腺疾病",
    displayEnglish: 'Disease of thymus, unspecified',
    predictedValue: 76
  },
  {
    code: 'I67.2',
    display: "腦動脈粥樣硬化",
    displayEnglish: 'Cerebral atherosclerosis',
    predictedValue: 30
  },
  {
    code: 'I63.5',
    display: "由於腦動脈未明的閉塞或狹窄而導致的腦梗塞",
    displayEnglish: 'Cerebral infarction due to unspecified occlusion or stenosis of cerebral arteries',
    predictedValue: 11
  }
];

export const mockDiagnosisSpecificList: Item[] = [
  {
    code: "A00-B99",
    display: "某些傳染病和寄生蟲病",
    displayEnglish: "Certain infectious and parasitic diseases",
    child: [
      {
        code: "A00-A09",
        display: "腸道傳染病",
        displayEnglish: "Intestinal infectious diseases",
        child: [
          {
            code: "A00",
            display: "霍亂",
            displayEnglish: "Cholera",
            child: []
          },
          {
            code: "A01",
            display: "傷寒和副傷寒",
            displayEnglish: "Typhoid and paratyphoid fevers",
            child: []
          }
        ]
      },
      {
        code: "A30-A49",
        display: "其他細菌性疾病",
        displayEnglish: "Other bacterial diseases",
        child: [
          {
            code: "A30",
            display: "麻風病[漢森病]",
            displayEnglish: "Leprosy [Hansen disease]",
            child: []
          },
          {
            code: "A33",
            display: "新生兒破傷風",
            displayEnglish: "Tetanus neonatorum",
            child: []
          },
          {
            code: "A34",
            display: "產科破傷風 ",
            displayEnglish: "Obstetrical tetanus",
            child: []
          }
        ]
      },
      {
        code: "B95-B98",
        display: "細菌、病毒和其他傳染源",
        displayEnglish: "Bacterial, viral and other infectious agents",
        child: [
          {
            code: "B95",
            display: "鏈球菌和葡萄球菌作為分類到其他章節的疾病的病因",
            displayEnglish: "Streptococcus and staphylococcus as the cause of diseases classified to other chapters",
            child: []
          },
          {
            code: "B98",
            display: "作為分類到其他章節的疾病原因的其他指定傳染源",
            displayEnglish: "Other specified infectious agents as the cause of diseases classified to other chapters",
            child: []
          }
        ]
      }
    ]
  },
  {
    code: "J00-J99",
    display: "呼吸系統疾病  ",
    displayEnglish: "Diseases of the respiratory system",
    child: [
      {
        code: "J09-J18",
        display: "流感和肺炎",
        displayEnglish: "Influenza and pneumonia",
        child: [
          {
            code: "J11",
            display: "流感，病毒未識別",
            displayEnglish: "Influenza, virus not identified",
            child: []
          },
          {
            code: "J12",
            display: "病毒性肺炎，未分類",
            displayEnglish: "Viral pneumonia, not elsewhere classified",
            child: []
          }
        ]
      },
      {
        code: "J90-J94",
        display: "其他胸膜疾病",
        displayEnglish: "Other diseases of pleura",
        child: [
          {
            code: "J90",
            display: "胸腔積液，未分類",
            displayEnglish: "Pleural effusion, not elsewhere classified",
            child: []
          },
          {
            code: "J91",
            display: "分類為其他疾病的胸腔積液",
            displayEnglish: "Pleural effusion in conditions classified elsewhere",
            child: []
          }
        ]
      }
    ]
  }
];

export const mockDiagnosisSpecificValue: Item[] = [
  {
    code: "A00-B99",
    display: "某些傳染病和寄生蟲病",
    displayEnglish: "Certain infectious and parasitic diseases",
    child: [
      {
        code: "A00-A09",
        display: "腸道傳染病",
        displayEnglish: "Intestinal infectious diseases",
        child: [
          {
            code: "A00",
            display: "霍亂",
            displayEnglish: "Cholera",
            child: [
              {
                code: "A00.0",
                display: "霍亂弧菌 01、霍亂生物變種",
                displayEnglish: "Classical cholera",
                child: []
              },
              {
                code: "A00.1",
                display: "由霍亂弧菌 01（Eltor 生物變種）引起的霍亂",
                displayEnglish: "Cholera eltor",
                child: []
              },
              {
                code: "A00.9",
                display: "霍亂，未明確",
                displayEnglish: "Cholera, unspecified",
                child: []
              }
            ]
          },
          {
            code: "A01",
            display: "傷寒和副傷寒",
            displayEnglish: "Typhoid and paratyphoid fevers",
            child: [
              {
                code: "A01.0",
                display: "傷寒",
                displayEnglish: "Typhoid fever",
                child: []
              },
              {
                code: "A01.1",
                display: "甲型副傷寒",
                displayEnglish: "Paratyphoid fever A",
                child: []
              },
              {
                code: "A01.2",
                display: "副傷寒乙型",
                displayEnglish: "Paratyphoid fever B",
                child: []
              },
              {
                code: "A01.3",
                display: "副傷寒丙型",
                displayEnglish: "Paratyphoid fever C",
                child: []
              },
              {
                code: "A01.4",
                display: "未明確的副傷寒",
                displayEnglish: "Paratyphoid fever, unspecified",
                child: []
              }
            ]
          }
        ]
      },
      {
        code: "A30-A49",
        display: "其他細菌性疾病",
        displayEnglish: "Other bacterial diseases",
        child: [
          {
            code: "A30",
            display: "麻風病[漢森病]",
            displayEnglish: "Leprosy [Hansen disease]",
            child: [
              {
                code: "A30.0",
                display: "不確定的麻風病",
                displayEnglish: "Indeterminate leprosy",
                child: []
              },
              {
                code: "A30.1",
                display: "結核性痲瘋病",
                displayEnglish: "Tuberculoid leprosy",
                child: []
              },
              {
                code: "A30.2",
                display: "交界性結核性痲瘋病",
                displayEnglish: "Borderline tuberculoid leprosy",
                child: []
              },
              {
                code: "A30.3",
                display: "邊緣性麻風病",
                displayEnglish: "Borderline leprosy",
                child: []
              }
            ]
          },
          {
            code: "A33",
            display: "新生兒破傷風",
            displayEnglish: "Tetanus neonatorum",
            child: []
          },
          {
            code: "A34",
            display: "產科破傷風 ",
            displayEnglish: "Obstetrical tetanus",
            child: []
          }
        ]
      },
      {
        code: "B95-B98",
        display: "細菌、病毒和其他傳染源",
        displayEnglish: "Bacterial, viral and other infectious agents",
        child: [
          {
            code: "B95",
            display: "鏈球菌和葡萄球菌作為分類到其他章節的疾病的病因",
            displayEnglish: "Streptococcus and staphylococcus as the cause of diseases classified to other chapters",
            child: [
              {
                code: "B95.0",
                display: "A 組鏈球菌，作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus, group A, as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "A95.1",
                display: "B 組鏈球菌，作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus, group B, as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B95.2",
                display: "D 型鏈球菌和腸球菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus group D and enterococcus as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B95.3",
                display: "肺炎鏈球菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus pneumoniae as the cause of diseases classified to other chapters",
                child: []
              }
            ]
          },
          {
            code: "B98",
            display: "作為分類到其他章節的疾病原因的其他指定傳染源",
            displayEnglish: "Other specified infectious agents as the cause of diseases classified to other chapters",
            child: [
              {
                code: "B98.0",
                display: "幽門螺旋桿菌 [H.pylori] 作為分類到其他章節的疾病的病因",
                displayEnglish: "Helicobacter pylori [H.pylori] as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B98.1",
                display: "創傷弧菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Vibrio vulnificus as the cause of diseases classified to other chapters",
                child: []
              }
            ]
          }
        ]
      }
    ]
  },
  {
    code: "J00-J99",
    display: "呼吸系統疾病  ",
    displayEnglish: "Diseases of the respiratory system",
    child: [
      {
        code: "J09-J18",
        display: "流感和肺炎",
        displayEnglish: "Influenza and pneumonia",
        child: [
          {
            code: "J11",
            display: "流感，病毒未識別",
            displayEnglish: "Influenza, virus not identified",
            child: [
              {
                code: "J11.0",
                display: "流感伴肺炎，病毒未鑑定",
                displayEnglish: "Influenza with pneumonia, virus not identified",
                child: []
              },
              {
                code: "J11.1",
                display: "具有其他呼吸道症狀的流感，病毒尚未確定",
                displayEnglish: "Influenza with other respiratory manifestations, virus not identified",
                child: []
              }
            ]
          },
          {
            code: "J12",
            display: "病毒性肺炎，未分類",
            displayEnglish: "Viral pneumonia, not elsewhere classified",
            child: [
              {
                code: "J12.0",
                display: "腺病毒肺炎",
                displayEnglish: "Adenoviral pneumonia",
                child: []
              },
              {
                code: "J12.1",
                display: "呼吸道合胞病毒肺炎",
                displayEnglish: "Respiratory syncytial virus pneumonia",
                child: []
              },
              {
                code: "J12.2",
                display: "副流感病毒肺炎",
                displayEnglish: "Parainfluenza virus pneumonia",
                child: []
              },
              {
                code: "J12.3",
                display: "人類偏肺病毒肺炎",
                displayEnglish: "Human metapneumovirus pneumonia",
                child: []
              }
            ]
          }
        ]
      },
      {
        code: "J90-J94",
        display: "其他胸膜疾病",
        displayEnglish: "Other diseases of pleura",
        child: [
          {
            code: "J90",
            display: "胸腔積液，未分類",
            displayEnglish: "Pleural effusion, not elsewhere classified",
            child: []
          },
          {
            code: "J91",
            display: "分類為其他疾病的胸腔積液",
            displayEnglish: "Pleural effusion in conditions classified elsewhere",
            child: []
          }
        ]
      }
    ]
  }
];

export const mockDiagnosisICDList: Item[] = [
  {
    code: "A00-B99",
    display: "某些傳染病和寄生蟲病",
    displayEnglish: "Certain infectious and parasitic diseases",
    child: [
      {
        code: "A00-A09",
        display: "腸道傳染病",
        displayEnglish: "Intestinal infectious diseases",
        child: [
          {
            code: "A00",
            display: "霍亂",
            displayEnglish: "Cholera",
            child: []
          },
          {
            code: "A01",
            display: "傷寒和副傷寒",
            displayEnglish: "Typhoid and paratyphoid fevers",
            child: []
          }
        ]
      },
      {
        code: "A30-A49",
        display: "其他細菌性疾病",
        displayEnglish: "Other bacterial diseases",
        child: [
          {
            code: "A30",
            display: "麻風病[漢森病]",
            displayEnglish: "Leprosy [Hansen disease]",
            child: []
          },
          {
            code: "A33",
            display: "新生兒破傷風",
            displayEnglish: "Tetanus neonatorum",
            child: []
          },
          {
            code: "A34",
            display: "產科破傷風 ",
            displayEnglish: "Obstetrical tetanus",
            child: []
          }
        ]
      },
      {
        code: "B95-B98",
        display: "細菌、病毒和其他傳染源",
        displayEnglish: "Bacterial, viral and other infectious agents",
        child: [
          {
            code: "B95",
            display: "鏈球菌和葡萄球菌作為分類到其他章節的疾病的病因",
            displayEnglish: "Streptococcus and staphylococcus as the cause of diseases classified to other chapters",
            child: []
          },
          {
            code: "B98",
            display: "作為分類到其他章節的疾病原因的其他指定傳染源",
            displayEnglish: "Other specified infectious agents as the cause of diseases classified to other chapters",
            child: []
          }
        ]
      }
    ]
  },
  {
    code: "J00-J99",
    display: "呼吸系統疾病  ",
    displayEnglish: "Diseases of the respiratory system",
    child: [
      {
        code: "J09-J18",
        display: "流感和肺炎",
        displayEnglish: "Influenza and pneumonia",
        child: [
          {
            code: "J11",
            display: "流感，病毒未識別",
            displayEnglish: "Influenza, virus not identified",
            child: []
          },
          {
            code: "J12",
            display: "病毒性肺炎，未分類",
            displayEnglish: "Viral pneumonia, not elsewhere classified",
            child: []
          }
        ]
      },
      {
        code: "J90-J94",
        display: "其他胸膜疾病",
        displayEnglish: "Other diseases of pleura",
        child: [
          {
            code: "J90",
            display: "胸腔積液，未分類",
            displayEnglish: "Pleural effusion, not elsewhere classified",
            child: []
          },
          {
            code: "J91",
            display: "分類為其他疾病的胸腔積液",
            displayEnglish: "Pleural effusion in conditions classified elsewhere",
            child: []
          }
        ]
      }
    ]
  }
];

export const mockDiagnosisICDValue: Item[] = [
  {
    code: "A00-B99",
    display: "某些傳染病和寄生蟲病",
    displayEnglish: "Certain infectious and parasitic diseases",
    child: [
      {
        code: "A00-A09",
        display: "腸道傳染病",
        displayEnglish: "Intestinal infectious diseases",
        child: [
          {
            code: "A00",
            display: "霍亂",
            displayEnglish: "Cholera",
            child: [
              {
                code: "A00.0",
                display: "霍亂弧菌 01、霍亂生物變種",
                displayEnglish: "Classical cholera",
                child: []
              },
              {
                code: "A00.1",
                display: "由霍亂弧菌 01（Eltor 生物變種）引起的霍亂",
                displayEnglish: "Cholera eltor",
                child: []
              },
              {
                code: "A00.9",
                display: "霍亂，未明確",
                displayEnglish: "Cholera, unspecified",
                child: []
              }
            ]
          },
          {
            code: "A01",
            display: "傷寒和副傷寒",
            displayEnglish: "Typhoid and paratyphoid fevers",
            child: [
              {
                code: "A01.0",
                display: "傷寒",
                displayEnglish: "Typhoid fever",
                child: []
              },
              {
                code: "A01.1",
                display: "甲型副傷寒",
                displayEnglish: "Paratyphoid fever A",
                child: []
              },
              {
                code: "A01.2",
                display: "副傷寒乙型",
                displayEnglish: "Paratyphoid fever B",
                child: []
              },
              {
                code: "A01.3",
                display: "副傷寒丙型",
                displayEnglish: "Paratyphoid fever C",
                child: []
              },
              {
                code: "A01.4",
                display: "未明確的副傷寒",
                displayEnglish: "Paratyphoid fever, unspecified",
                child: []
              }
            ]
          }
        ]
      },
      {
        code: "A30-A49",
        display: "其他細菌性疾病",
        displayEnglish: "Other bacterial diseases",
        child: [
          {
            code: "A30",
            display: "麻風病[漢森病]",
            displayEnglish: "Leprosy [Hansen disease]",
            child: [
              {
                code: "A30.0",
                display: "不確定的麻風病",
                displayEnglish: "Indeterminate leprosy",
                child: []
              },
              {
                code: "A30.1",
                display: "結核性痲瘋病",
                displayEnglish: "Tuberculoid leprosy",
                child: []
              },
              {
                code: "A30.2",
                display: "交界性結核性痲瘋病",
                displayEnglish: "Borderline tuberculoid leprosy",
                child: []
              },
              {
                code: "A30.3",
                display: "邊緣性麻風病",
                displayEnglish: "Borderline leprosy",
                child: []
              }
            ]
          },
          {
            code: "A33",
            display: "新生兒破傷風",
            displayEnglish: "Tetanus neonatorum",
            child: []
          },
          {
            code: "A34",
            display: "產科破傷風 ",
            displayEnglish: "Obstetrical tetanus",
            child: []
          }
        ]
      },
      {
        code: "B95-B98",
        display: "細菌、病毒和其他傳染源",
        displayEnglish: "Bacterial, viral and other infectious agents",
        child: [
          {
            code: "B95",
            display: "鏈球菌和葡萄球菌作為分類到其他章節的疾病的病因",
            displayEnglish: "Streptococcus and staphylococcus as the cause of diseases classified to other chapters",
            child: [
              {
                code: "B95.0",
                display: "A 組鏈球菌，作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus, group A, as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "A95.1",
                display: "B 組鏈球菌，作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus, group B, as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B95.2",
                display: "D 型鏈球菌和腸球菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus group D and enterococcus as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B95.3",
                display: "肺炎鏈球菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Streptococcus pneumoniae as the cause of diseases classified to other chapters",
                child: []
              }
            ]
          },
          {
            code: "B98",
            display: "作為分類到其他章節的疾病原因的其他指定傳染源",
            displayEnglish: "Other specified infectious agents as the cause of diseases classified to other chapters",
            child: [
              {
                code: "B98.0",
                display: "幽門螺旋桿菌 [H.pylori] 作為分類到其他章節的疾病的病因",
                displayEnglish: "Helicobacter pylori [H.pylori] as the cause of diseases classified to other chapters",
                child: []
              },
              {
                code: "B98.1",
                display: "創傷弧菌作為分類到其他章節的疾病的病因",
                displayEnglish: "Vibrio vulnificus as the cause of diseases classified to other chapters",
                child: []
              }
            ]
          }
        ]
      }
    ]
  },
  {
    code: "J00-J99",
    display: "呼吸系統疾病  ",
    displayEnglish: "Diseases of the respiratory system",
    child: [
      {
        code: "J09-J18",
        display: "流感和肺炎",
        displayEnglish: "Influenza and pneumonia",
        child: [
          {
            code: "J11",
            display: "流感，病毒未識別",
            displayEnglish: "Influenza, virus not identified",
            child: [
              {
                code: "J11.0",
                display: "流感伴肺炎，病毒未鑑定",
                displayEnglish: "Influenza with pneumonia, virus not identified",
                child: []
              },
              {
                code: "J11.1",
                display: "具有其他呼吸道症狀的流感，病毒尚未確定",
                displayEnglish: "Influenza with other respiratory manifestations, virus not identified",
                child: []
              }
            ]
          },
          {
            code: "J12",
            display: "病毒性肺炎，未分類",
            displayEnglish: "Viral pneumonia, not elsewhere classified",
            child: [
              {
                code: "J12.0",
                display: "腺病毒肺炎",
                displayEnglish: "Adenoviral pneumonia",
                child: []
              },
              {
                code: "J12.1",
                display: "呼吸道合胞病毒肺炎",
                displayEnglish: "Respiratory syncytial virus pneumonia",
                child: []
              },
              {
                code: "J12.2",
                display: "副流感病毒肺炎",
                displayEnglish: "Parainfluenza virus pneumonia",
                child: []
              },
              {
                code: "J12.3",
                display: "人類偏肺病毒肺炎",
                displayEnglish: "Human metapneumovirus pneumonia",
                child: []
              }
            ]
          }
        ]
      },
      {
        code: "J90-J94",
        display: "其他胸膜疾病",
        displayEnglish: "Other diseases of pleura",
        child: [
          {
            code: "J90",
            display: "胸腔積液，未分類",
            displayEnglish: "Pleural effusion, not elsewhere classified",
            child: []
          },
          {
            code: "J91",
            display: "分類為其他疾病的胸腔積液",
            displayEnglish: "Pleural effusion in conditions classified elsewhere",
            child: []
          }
        ]
      }
    ]
  }
];
