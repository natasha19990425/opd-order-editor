import { Injectable, inject } from '@angular/core';
import { JetStreamWsService } from '@his-base/jetstream-ws';
import { Item } from '@his-directive/multi-slideway/dist/multi-slideway';

@Injectable({
  providedIn: 'root'
})
export class SlidewayCartService {

  #jetStreamWsService: JetStreamWsService = inject(JetStreamWsService);

  async getDrugLeaf(_id: string): Promise<Item[]> {
    return await this.#jetStreamWsService.request('medOrder.catalog.treeItems', _id);
  }
}
