import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SlidewayCartService } from './slideway-cart.service';

import { Coding } from '@his-base/datatypes';
import { MedTree } from '@his-viewmodel/med-order/dist';
import { CartComponent } from '@his-directive/cart/dist/cart';
import { Item, MultiSlidewayComponent } from '@his-directive/multi-slideway/dist/multi-slideway';

import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';

const primengGroup = [ButtonModule, DialogModule, InputTextModule];

@Component({
  selector: 'his-slideway-cart',
  standalone: true,
  imports: [CommonModule, CartComponent, MultiSlidewayComponent, FormsModule, primengGroup],
  templateUrl: './slideway-cart.component.html',
  styleUrls: ['./slideway-cart.component.scss']
})
export class SlidewayCartComponent {

  @Input() visible: boolean = false;
  @Input()
  set medTree(value: MedTree) {
    if (value && value.child && value.child.length) {
      this.drugTree = value.child;
      this.#medTree = value;
    }
  }
  get medTree(): MedTree {
    return this.#medTree;
  }
  #medTree: MedTree = {} as MedTree;

  @Output() cartCancel = new EventEmitter<void>();
  @Output() addDrugOrder = new EventEmitter<Coding[]>();

  cartItem: Coding = {} as Coding;
  cartItems: Coding[] = [];
  drugTree: Item[] = [];
  drugLeaf: Item = {} as Item;

  #slidewayCartService: SlidewayCartService = inject(SlidewayCartService);

  async onNodeChange(key: Coding) {
    this.drugLeaf = {
      code: key.code,
      display: key.display,
      child: await this.#slidewayCartService.getDrugLeaf(key.code)
    };
  }

  onCancelClick() {
    this.cartCancel.emit();
  }

  addOrderClick() {
    this.addDrugOrder.emit(this.cartItems);
  }

  onItemSelect(result: Item) {
    this.cartItem = new Coding({ code: result.code, display: result.display });
  }
}
